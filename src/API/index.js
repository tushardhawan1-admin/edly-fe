import HELPERS from "../utils/helper";
const API_Manager = {
  signup: (data) => {
    const form_data = HELPERS.converToFormData(data);
    return HELPERS.request({
      url: `users/signup/`,
      method: "POST",
      data: form_data,
      headers: { "Content-Type": "multipart/form-data" },
    });
  },
  login: (data) => {
    const form_data = HELPERS.converToFormData(data);
    return HELPERS.request({
      url: "users/login/",
      method: "POST",
      data: form_data,
      headers: { "Content-Type": "multipart/form-data" },
    });
  },

  forgotPassword: (data) => {
    const form_data = HELPERS.converToFormData(data);
    return HELPERS.request({
      url: "users/forgot-password/",
      method: "PATCH",
      data: "form_data",
      headers: { "Content-Type": "multipart/form-data" },
    });
  },

  changePassword: (id, data) => {
    const form_data = HELPERS.converToFormData(data);
    return HELPERS.secureRequest({
      url: `users/change-password/${id}`,
      method: "PATCH",
      data: form_data,
      // headers: {'Content-Type':'multipart/form-data'}
    });
  },

  studentUpdate: (id, data) => {
    console.log(id);
    const form_data = HELPERS.converToFormData(data);
    return HELPERS.secureRequest({
      url: `/students/${id}`,
      method: "PATCH",
      data: form_data,
    });
  },

  studentDetail: (id) => {
    console.log(id);
    return HELPERS.secureRequest({
      url: `/students/${id}`,
      method: "GET",
    });
  },

  studentCreateIsa: (id, data) => {
    const form_data = HELPERS.converToFormData(data);
    return HELPERS.secureRequest({
      url: `/students/${id}/isa`,

      method: "POST",
      data: form_data,
    });
  },

  studentChangePassword: (id, data) => {
    const form_data = HELPERS.converToFormData(data);
    return HELPERS.secureRequest({
      url: `/users/change-password/${id}`,
      method: "PATCH",
      data: form_data,
    });
  },

  studentIsa: (id) => {
    return HELPERS.secureRequest({
      url: `student/${id}/isa`,
      method: "GET",
    });
  },
};

export default API_Manager;
