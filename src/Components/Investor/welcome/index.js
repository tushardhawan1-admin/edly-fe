import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
} from "antd";
import email from "../../../Assets/images/email-check.svg";
import checkimage from "../../../Assets/images/check-email-image_2021-02-16/check-email-image.png";


const { Content } = Layout;

class Welcome extends Component {
  state = {};
  render() {
    return (
        <div className="contain">
        <Row>
            <Col className="rightcol" span={9}>
            <Row>
                <Col>
                <Image src={email} preview={false} className="mail"/></Col>
            </Row>
            <Row className="Check-Email">
                <Col>
                Check Email</Col>
            </Row>
            <Row className="Thank-you-for-taking"><Col>
            <p>Thank you for taking your first step to investing in higher education!</p>
            <p>
We’ve emailed you a link to confirm your email.</p>
</Col>
            </Row>
            <Row className="trouble"><Col>
            
            Having troubles signing up? Contact us <a>here</a>.
            </Col>
            </Row>
            </Col >

            <Col span={15} className="checkimage">
            <Image src={checkimage} preview={false}/>
            </Col>
        
        
        </Row>
        </div>
      
    );
  }
}
export default Welcome;
