import { Layout } from "antd";
import React from 'react';
import "./index.css";
import Head from "../../Layout/Header/index";
import Foot from "../../Layout/Footer/index";

const { Content } = Layout;

class Contents extends React.Component {
  render()
  { 
  return (
    <>
      <Head />
      <Content className="contents">
        {this.props.children}
      </Content>
      <Foot />
    </>
  );
}
}

export default Contents;
