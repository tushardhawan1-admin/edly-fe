import "antd/dist/antd.css";
import "./index.css";
import lock from "../../../Assets/images/icon-bold-icon-lock-bold.svg";
import bulb from "../../../Assets/images/icon-bold-icon-lightbulb-bold.svg";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
} from "antd";

const { Content } = Layout;

class CreatePassword extends Component {
  state = {};
  render() {
    return (
      <Row>
        <Col className="passwordcol" span={24}>
          <Row><Col span={24}>
            <div className="lock-password">
              <Image src={lock} preview={false} />
            
            </div>
            </Col>
          </Row>
          <Row><Col>
          
              <div className="lockt">Create your password</div>
          </Col></Row>
          <Form layout='vertical'>
          <Row>
            <Col span={22} className="inputs">
            <Form.Item>
              <Input type="password" size="large" placeholder="Password"/>
            </Form.Item>
            </Col>
          </Row>

          <Row className="passwordquality">
            <Col className="quality">
            Strong</Col>
          </Row>

          <Row className="passmessage">
            <Col className="bulb" span={4}>
            <Image src={bulb} preview={false}/></Col>
            <Col span={18} className="message">Be sure to use a combination of upper and lower case letters, numbers, and symbols.</Col>
          </Row>
          <Row>
            <Col span={24} > 
          <Button type="primary" shape="round" className="sign"  >
            <div className="create">
          Create Password
          </div>
                </Button>
                </Col>
          </Row>
          </Form>

          
          
        </Col>
      </Row>
    );
  }
}
export default CreatePassword;
