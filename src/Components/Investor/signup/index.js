import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import lock from "../../../Assets/images/icon-bold-icon-lock-bold.svg";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
} from "antd";
import Checkbox from "antd/lib/checkbox/Checkbox";
import money from '../../../Assets/images/icon-money-sign.svg';
import diverse from '../../../Assets/images/icon-diversification.svg';
import cash from '../../../Assets/images/icon-cash-flow.svg';
import invest from '../../../Assets/images/icon-invest-protect.svg';
import student from '../../../Assets/images/icon-graduate-student.svg';
import { Link } from "react-router-dom";

const { Content } = Layout;

class Forms extends Component {
  state = {};
  render() {
    return (
      <Row>
        <Col  span={10}>
          <div className="form">
          <p className="heading">Invest in Student Success</p>
          <Form layout="vertical  ">
            <Row>
              <Col span={24} className="inputs">
                <Form.Item label="First Name">
                  <Input size="large" />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={24} className="inputs">
                <Form.Item label="Last Name">
                  <Input size="large" />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={24} className="inputs">
                <Form.Item label="Email Address">
                  <Input size="large" />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={24} className="inputs">
                <Form.Item label="Phone Number">
                  <Input size="large" placeholder="1 _ _ _ - _ _ _ - _ _ _ _" />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={2} offset={1}>
                <div className="lock">
                  <Image src={lock} preview={false} />
                </div>
              </Col>
              <Col span={21}>
                <div className="info-text">
                  We will send an email with instructions to create a secure
                  password.
                </div>
              </Col>
            </Row>
            <Row>
              <Col span={1}>
                <Checkbox className="check-signup"/>
                </Col>
                <Col span={23}>
                  <p className="check-text-signup">
                    By opening your Edly account, you agree to our{" "}
                    <a>terms of service</a>, and <a>privacy policy</a>.
                  </p>
                
              </Col>
            </Row>

            <Row>
              <Col span={24}className="signup-investor-signup">
                <Button type="primary" shape="round" className="signup-investor">
                  Sign up
                </Button>
              </Col>
            </Row>

            <Row>
              <Col span={24} className="already">
                Already have an account? <a>Login</a>
              </Col>
            </Row>
          </Form>
          </div>
        </Col>

        <Col className='rightcolumn' span={10}>
        <Row className='righttitle'>
        Why investors like Edly:
        </Row>
        <Row className='money'>
          <Col >
            <Image src={money} preview={false}/>
          </Col>
          <Col span={18}>
          <Row className="rightcd">
          Target returns of 8% -14% 
          </Row>
          <Row className='rightsub'>
          (8% principal protected with a treasury strip)
          </Row>
          </Col>
        </Row>

        <Row className='diverse'>
          <Col >
            <Image src={diverse} preview={false}/>
          </Col>
          <Col className="rightcd" span={18}>
          
          Diversification across schools and job types
          
          </Col>
        </Row>


        <Row className="diverse">
          <Col>
            <Image src={cash} preview={false}/>
          </Col>
          <Col className="rightcd" span={18}>
          
          Monthly cash flow
          
          </Col>
        </Row>

        <Row  className='diverse'>
          <Col>
            <Image src={invest} preview={false}/>
          </Col>
          <Col className="rightcd" span={18}>
          
          Investors can choose to have principal protection
          
          </Col>
        </Row>


        <Row className='diverse'>
          <Col >
            <Image src={student} preview={false}/>
          </Col>
          <Col className="rightcd" span={18}>
          
          Help high-potential students get access to education
          
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Divider className="right-divider"></Divider>
          </Col>
        </Row>
        <Row>
          <Col span={24} className="are-you-and">

          Are you and Advisor?
          </Col>
        </Row>

        <Row>
          <Col span={22} className="edly-works">
          Edly works with all types of investors and advisors. 
          For more information on how to participate in Edly’s offerings,<Link> click here.</Link>
          </Col>
        </Row>
        
        
        </Col>
      </Row>
    );
  }
}
export default Forms;
