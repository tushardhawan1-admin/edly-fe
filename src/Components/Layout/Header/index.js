import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
} from "antd";
import logo from "../../../Assets/images/Logo/Logo.svg";
import {Link, Route} from 'react-router-dom';

const { Header } = Layout;

class Head extends Component {
  state = {};
  render() {
    return (
      <Header
        style={{ position: "fixed", zIndex: 1, background: "#ffffff" }}
        className="head"
      >
        <Row style={{ height: "inherit" }} align="middle">
          <Col span={3} style={{ display: "flex", justifyContent: "left" }}>
            <Image src={logo} preview={false} className="edly" />
          </Col>
          <Col span={7}></Col>
          <Col span={10} className="headnav">
            {/* <Menu mode="horizontal" style={{ align: "right" }} inlineCollapsed={false}>
              <Menu.Item>How it Works</Menu.Item>
              <Menu.Item>Invest</Menu.Item>
              <Menu.Item>Resources</Menu.Item>
              <Menu.Item>About</Menu.Item>
              <Menu.Item>News</Menu.Item>
            </Menu> */}
            <Link to="#" className="header-links">
              How it Works
            </Link>
            <Link to="#" className="header-links">
              Invest
            </Link>
            <Link to="#" className="header-links">
              Resources
            </Link>
            <Link to="#" className="header-links">
              About
            </Link>
            <Link to="#" className="header-links" style={{paddingRight:'0'}}>
              News
            </Link>
          </Col>
          <Col span={2}>
            <Divider type="vertical" style={{paddingRight:'23px'}} />
            <Typography.Link className="Login">Login</Typography.Link>
          </Col>
          <Col span={2}>
            <Button type="primary" shape="round" className='signUp'>
              Sign Up
            </Button>
          </Col>
        </Row>
      </Header>
    );
  }
}
export default Head;
