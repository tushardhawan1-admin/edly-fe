import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
} from "antd";
import logo from "../../../Assets/images/Logo/Logo.svg";
import tweet from "../../../Assets/images/icon-regular-icon-social-twitter.svg";
import linked from "../../../Assets/images/icon-regular-icon-social-linkedin.svg";
import { Link } from "react-router-dom";
const { Footer } = Layout;

class Foot extends Component {
  state = {};
  render() {
    return (
      <Layout.Footer className="Background">
        <Row>
          <Col span={6}>
            <div className="logo">
              <Image src={logo} preview={false} />
            </div>

            <div className="footlogotext">
              <div style={{ marginBottom: "16px" }}>
                555 Pleasantville Road, Suite N202 Briarcliff Manor, NY 10510
                Call&nbsp;
              </div>
              <div style={{ marginBottom: "6px" }}>
                Call <span>(914)755-8299</span>
              </div>
              <div>
                Email <span>info@edly.info</span>
              </div>
              <div className="contact">
                <a className="conta">Contact Us</a>
              </div>
            </div>
          </Col>
          <Col span={2}></Col>
          <Col span={4}>
            <div className="footcol">Product</div>
            <div className="footer-links-div">
              <Link className="footerLinks">How it Works</Link>
            </div>
            <div className="footer-links-div">
              <Link className="footerLinks">Invest</Link>
            </div>
            <div className="footer-links-div">
              <Link className="footerLinks">Login</Link>
            </div>
            <div className="footer-links-div">
              <Link className="footerLinks">Get Started</Link>
            </div>
          </Col>
          <Col span={4}>
            <div className="footcol">Company</div>
            <div className="footer-links-div">
              <Link className="footerLinks">Our Solution</Link>
            </div>
            <div className="footer-links-div">
              <Link className="footerLinks">Team</Link>
            </div>
            <div className="footer-links-div">
              <Link className="footerLinks">News</Link>
            </div>
          </Col>
          <Col span={4}>
            <div className="footcol">Resources</div>
            <div className="footer-links-div">
              <Link className="footerLinks">Help & FAQs</Link>
            </div>
            <div className="footer-links-div">
              <Link className="footerLinks">ISAs 101</Link>
            </div>
            <div className="footer-links-div">
              <Link className="footerLinks">Coding Schools 101</Link>
            </div>
            <div className="footer-links-div">
              <Link className="footerLinks">Industry Materials</Link>
            </div>
          </Col>
          <Col span={4}>
            <div className="footcol">Connect</div>
            <div className="links">
              <div shape="circle" className="footerButton">
                <Image src={tweet} preview={false} />
              </div>
              <div shape="circle" className="footerButton">
                <Image src={linked} preview={false} />
              </div>
            </div>
          </Col>
        </Row>
        <Divider className="footdivider" />
        <Row className="terms">
          <span className='terms-text'>Terms of Service</span>
          <span className='terms-text'>Privacy Policy</span>
          <span className='terms-text'>Legal</span>
        </Row>
        <Row className="copy">
          <Col>Copyright © 2020 Edly LLC. All rights reserved.</Col>
        </Row>
        <Row className="right">
          <Col>
            From time to time, edly will make opportunities for investments in
            ISAs available to investors who are qualified as “Accredited
            Investors” (as defined below) and who have expressed an interest in
            learning about such investment opportunities. No investment
            opportunity is being offered at this time by edly and all
            prospective investors will be given opportunities in the future
            after verification of their status and an expression of specific
            interest. When investment opportunities are being offered, a
            Confidential Private Placement Memorandum (a “PPM”) will be
            furnished to approved investors on a confidential basis. Sales of
            the securities may not be consummated unless prospective investors
            are provided with a PPM. In particular, you should carefully read
            the “Risk Factors” set forth in the PPM. The information contained
            in the PPM may not be provided to persons who are not directly
            concerned with an investor’s decision regarding the investment
            offered hereby. Investors should make their own investigations and
            evaluations of any securities offered. The securities, when offered,
            will not be registered under the Securities Act of 1933, as amended
            (the “Securities Act”), or any other securities laws. They will be
            offered for investment pursuant to the exemption from registration
            provided under Section 4(a)(2) of the Securities Act and exemptions
            from the registration requirements of applicable state securities
            laws only to persons who are “Accredited Investors,” as defined in
            Regulation D under the Securities Act. There will be no public
            market for the offered securities.
          </Col>
        </Row>
        <Row className="right">
          <Col>
            This website, and the information being provided to prospective
            investors at this time, is not an offer to sell or a solicitation of
            an offer to buy any securities that may be offered in the future by
            edly, nor shall any such securities be offered or sold to any person
            in any jurisdiction in which such offer, solicitation, purchase, or
            sale would be unlawful under the securities laws of such
            jurisdiction.
          </Col>
        </Row>
        <Row className="right">
          <Col>
            ANY SECURITIES OFFERED BY EDLY WILL NOT HAVE BEEN RECOMMENDED BY THE
            U.S. SECURITIES AND EXCHANGE COMMISSION OR ANY STATE SECURITIES
            COMMISSION OR REGULATORY AUTHORITY. ANY SECURITIES OFFERED BY EDLY
            WILL BE SUBJECT TO RESTRICTIONS ON TRANSFERABILITY AND RESALE, AND
            MAY NOT BE TRANSFERRED OR RESOLD EXCEPT AS PERMITTED UNDER THE
            SECURITIES ACT AND APPLICABLE STATE SECURITIES LAWS, PURSUANT TO
            REGISTRATION OR AN EXEMPTION THEREFROM. INVESTORS SHOULD BE AWARE
            THAT THEY MAY BE REQUIRED TO BEAR THE FINANCIAL RISKS OF THEIR
            INVESTMENT IN THE SECURITIES FOR AN INDEFINITE TIME. CERTAIN
            INFORMATION CONTAINED IN THIS WEBSITE AND IN THE PPM TO BE DELIVERED
            IN THE FUTURE CONSTITUTES AND WILL CONSTITUTE “FORWARD-LOOKING
            STATEMENTS,” WHICH CAN BE IDENTIFIED BY THE USE OF FORWARD-LOOKING
            TERMINOLOGY SUCH AS “MAY,” “WILL,” “SHOULD,” “EXPECT,” “ANTICIPATE,”
            “ESTIMATE,” “INTEND,” “CONTINUE” OR “BELIEVE” TO THE NEGATIVES
            THEREOF OR OTHER VARIATIONS THEREON OR COMPARABLE TERMINOLOGY. DUE
            TO VARIOUS RISKS AND UNCERTAINTIES, ACTUAL EVENTS OR RESULTS FROM
            THE ACTUAL PERFORMANCE OF THE SECURITIES MAY DIFFER MATERIALLY FROM
            THOSE REFLECTED OR CONTEMPLATED IN SUCH FORWARD-LOOKING STATEMENTS.
          </Col>
        </Row>
      </Layout.Footer>
    );
  }
}
export default Foot;
