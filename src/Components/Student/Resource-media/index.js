import "antd/dist/antd.css";
import "./index.css";
import {
  BankOutlined,
  DownOutlined,
  BookOutlined,
  CalendarOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
  Select,
  Steps,
} from "antd";
import Empty_circle from "../table-symbols/empty_circle/index";
import Filled_circle from "../table-symbols/filled_circle/index";

import landing_image from "../../../Assets/images/landing-image.png";
import landing_image2 from "../../../Assets/images/landing-image-2.png";
import landing_image3 from "../../../Assets/images/landing-image-3.png";

import dollar_bag from "../../../Assets/images/bag-dollar.png";
import user_heart from "../../../Assets/images/user-heart.png";
import stocks_up from "../../../Assets/images/stocks-up.png";
import journal from "../../../Assets/images/journal-pencil.svg";

import group_media from "../../../Assets/images/group-media.png";
import media1 from "../../../Assets/images/media1.png";
import media2 from "../../../Assets/images/media2.png";
import Search from "antd/lib/transfer/search";

const { Content } = Layout;
const { Option } = Select;
const { Step } = Steps;

class Landing_Page extends Component {
  isaForm = React.createRef();
  state = {
    current: 0,
  };

  onChange = (current) => {
    console.log("onChange:", current);
    this.setState({ current });
  };

  render() {
    const { current } = this.state;
    const desc_title = [
      "Apply",
      "Get Approved",
      "Receive Funding",
      "Go to school",
      "Get a Job",
      "Never Stress",
    ];
    const descinfo = [
      "Apply to see if you're eligible in 1 minute or less and don't worry, your application information will never effect your credit score.",
      "Edly processes applications and returns students with ISA contract terms. Once you're approved, Edly sets you up with your account servicer.",
      "Edly Sends directly to your school. You don't need to lift a finger.",
      "Keep your head down and your grades up! Focus on your Education without ever paying a dime.",
      "Once you land a job making above a defined minimum (Usually $30-40K per year) You'll Start paying a percentage of your income back to Edly.",
      "If life throws lemons, Your ISA payments are put on hold.",
    ];
    const desc = (i) => (
      <>
        <Image src={journal} style={{ fontSize: "20px" }} />
        <p>{descinfo[i]}</p>
      </>
    );

    return (
      <div className="desktop-landing">
        <div className="main-content">
          <Row>
            <Col xs={24} md={12} lg={12}>
              <Image
                src={group_media}
                preview={false}
                className="landing-image"
              />
            </Col>
          </Row>
          <div className="resource-media">
            <Row justify='space-between'className="search-bar">
              <Col span={19} >
                <Input
                  placeholder="Search"
                  size="large"
                  prefix={<SearchOutlined />}
                  allowClear
                  // enterButton="Go"
                  className="first-input"
                  bordered={false}
                />
              </Col>
              <Col span={4}>
                <Button type='primary' size="large">
                    Go
                </Button>
              </Col>
            </Row>

            <Row>
              <Col span={24} className="resource-image-3">
                <Image
                  src={media1}
                  preview={false}
                  className="resource-image-3"
                />
              </Col>
              <Col span={24} className="title">
                How to fund your education smartly?
              </Col>
              <Col span={24} className="feature-desc">
                Check you inbox for an email from Edly admin and verify by
                clicking in the link.Check you inbox for an email from Edly
                admin and verify by clicking in the link.
              </Col>
            </Row>
            <Row>
              <Col span={24} className="resource-image-3">
                <Image
                  src={media2}
                  preview={false}
                  className="resource-image-3"
                />
              </Col>
              <Col span={24} className="title">
                Don't let your savings decide which university to study in.
              </Col>
              <Col span={24} className="feature-desc">
                Check you inbox for an email from Edly admin and verify by
                clicking in the link.Check you inbox for an email from Edly
                admin and verify by clicking in the link.
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}
export default Landing_Page;
