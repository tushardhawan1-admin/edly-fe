import "antd/dist/antd.css";
import "./index.css";
import {
  PlusCircleFilled,
  MinusCircleFilled,
  ArrowDownOutlined,
  ArrowUpOutlined,
} from "@ant-design/icons";
import React, { Component, useState } from "react";
import { Link, Route } from "react-router-dom";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
  Select,
  Steps,
  Affix,
} from "antd";

import go_back from "../../../Assets/images/send-forward.png";
import funding_graph from "../../../Assets/images/funding-graph.png";

const Isa_eligibility =(props)=> {
  const [funding,setFunding]=useState(15000);
  
  // const fundDecrease=(funding)=>{
  //   funding=funding-1000;
  //   setFunding(funding);
  // };

  // const fundIncrease=(funding)=>{
  //   funding=funding+1000;
  // };
  
    return (
      <div className="desktop-landing">
        <div className="go-back-change">
          <Button type="text" onClick={props.history.goBack}>
            <Image src={go_back} preview={false} />
            <span className="back-go"> Go Back</span>
          </Button>
        </div>
        <div className="main-content">
          <Row>
            <Col className="isa-title-content-1" span={24}>
              seeking funding of
            </Col>
          </Row>

          <Row justify="center" className="funding-tab" align="middle">
            <Col span={6}>
              <Button type="text" className="minus" shape="circle" onClick={() => setFunding(funding - 1000)}>
              <MinusCircleFilled
                style={{ color: "#4E8ADE", fontSize: "26px" }}
              />
              </Button>
            </Col>
            <Col className="funding" span={12}>
              $ {funding}
            </Col>
            <Col span={6}>
              <Button type="text" shape="circle" className="plus" onClick={() => setFunding(funding + 1000)}>
              <PlusCircleFilled
                style={{ color: "#4E8ADE", fontSize: "26px" }}
              /></Button>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Image
                src={funding_graph}
                preview={false}
                className="funding-graph"
              />
            </Col>
          </Row>
          <div style={{ paddingLeft: "30px", paddingRight: "30px" }}>
            <Row className="isa-variable">
              <Col className="title" span={12}>
                ISA Variables
              </Col>

              <Col span={12} className="view-full-terms">
                <Link>View Full terms</Link>
              </Col>
            </Row>

            <Row gutter={64}>
              <Col className="isa-percent" span={12}>
                <div className='isa-percent-1'>
                  5.70%
                  <div className="statistic">
                    <ArrowUpOutlined />
                    <p>0.20%</p>
                  </div>
                </div>

                <Row className="stat-title">
                  <Col span={24}>
                    <p>ISA Percentage</p>
                  </Col>
                </Row>
              </Col>

              <Col className="isa-percent" span={12}>
              <div className='isa-percent-1'>
                74
                <div className="statistic">
                  <ArrowUpOutlined />
                  <p>14</p>
                </div>
                </div>
                <Row className="stat-title">
                  <Col span={24}>
                    <p>Maximum Payments</p>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row gutter={64}>
              <Col className="isa-percent" span={12}>
              <div className='isa-percent-1'>
                $38,000/M
                <div className="statistic">
                  <ArrowUpOutlined />
                  <p>$2000</p>
                </div>
                </div>
                <Row className="stat-title">
                  <Col span={24}>
                    <p>Minimum Income Threshold</p>
                  </Col>
                </Row>
              </Col>

              <Col className="isa-percent" span={12}>
              <div className='isa-percent-1'>
                20 Mons
                <div className="statistic">
                  <ArrowUpOutlined />
                  <p>40</p>
                </div>
                </div>
                <Row className="stat-title">
                  <Col span={24}>
                    <p>Payment Window</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }

export default Isa_eligibility;
