import {
  BankOutlined,
  BookOutlined,
  DollarOutlined,
  FlagOutlined,
  ReadOutlined,
  WalletOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import { Button, Col, DatePicker, Form, Input, Row, Select } from "antd";
import "antd/dist/antd.css";
import moment from "moment";
import React, { useEffect, useState, useRef } from "react";
import { withRouter } from "react-router-dom";
import API_Manager from "../../../API/index";
import "./index.css";

const { Option } = Select;

const Create_ISA_S = (props) => {
  const formRef = useRef(null);

  const [formValues, setFormValues] = useState({
    school_name: "",
    major: "",
    degree_type: "",
    funding_semester: "",
    funding_amount: "",
    citizenship_status: "",
    funding_year: "",
    month: "",
    grad_year: "",
    grad_month: "",
  });

  const createIsa = (values) => {
    const fyear = moment(values["funding_year"]).format("YYYY");
    const fmonth = moment(values["month"]).format("MM");
    const gyear = moment(values["grad_year"]).format("YYYY");
    const gmonth = moment(values["grad_month"]).format("MM");
    values["funding_year"] = fyear;
    values["month"] = fmonth;
    values["grad_year"] = gyear;
    values["grad_month"] = gmonth;
    values["verification_status"] = 20;
    values["type"] = "Student";
    const id = localStorage.getItem("student_id");
    console.log("form values after submit:", values);
    API_Manager.studentCreateIsa(id, values)
      .then((result) => {
        console.log("Isa created", result);
        console.log(localStorage.getItem("Token"));
        console.log(localStorage.getItem("id"));
        // window.location.href="/student/landing"
        props.history.push("student/isa-eligibility");
      })
      .catch((error) => console.log(error));
  };

  const onFormChange = (values, all_values) => {
    console.log("form values:", values, all_values);
    const key = Object.keys(values)[0];
    const new_form = formValues;
    new_form[key] = values[key];
    setFormValues(new_form);
    console.log("state values:", formValues, key, new_form);
  };

  const school_name = localStorage.getItem("school_name");
  const current_year = moment(localStorage.getItem("current_year"));
  const major = localStorage.getItem("major");
  const funding_year = moment(localStorage.getItem("funding_year"));

  useEffect(() => {
    formRef.current.setFieldsValue({
      school_name,
      major,
      funding_year,
      current_year,
    });
  }, [major, current_year, funding_year, school_name]);

  return (
    <div
      className="desktop-landing"
      onLoad={console.log(
        localStorage.getItem("funding_year")
          ? moment(localStorage.getItem("funding_year")).format("YYYY")
          : moment(null)
      )}
    >
      <div className="main-content">
        <Row justify="center">
          <Col span={24} className="title" style={{ marginTop: "30px" }}>
            <div className="feat">Initiate ISA Request</div>
          </Col>
        </Row>
        <div className="isa-form" style={{ marginTop: "0" }}>
          <Form
            onFinish={createIsa}
            onValuesChange={onFormChange}
            ref={formRef}
          >
            <Row>
              <Col span={24}>
                <div className="custom-icon-wrapper">
                  <Form.Item
                    name="school_name"
                    initialValue={
                      localStorage.getItem("school_name")
                        ? localStorage.getItem("school_name")
                        : null
                    }
                  >
                    <Select
                      showSearch
                      className={
                        formValues.school_name.length > 0
                          ? "university university-focus-within"
                          : "university"
                      }
                      size="large"
                      bordered={false}
                      optionLabelProp="key"
                      name="school_name"
                      placeholder="Name of Institute"
                    >
                      <Option key="University of USA" key="University of USA">
                        University of USA
                      </Option>
                    </Select>
                  </Form.Item>
                  <BankOutlined className="custom-icons" />
                </div>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <div className="custom-icon-wrapper">
                  <Form.Item
                    name="major"
                    initialValue={
                      localStorage.getItem("major")
                        ? localStorage.getItem("major")
                        : null
                    }
                  >
                    <Select
                      showSearch
                      className={
                        formValues.major.length > 0
                          ? "university university-focus-within"
                          : "university"
                      }
                      size="large"
                      name="major"
                      optionLabelProp="label"
                      bordered={false}
                      placeholder="Major Field"
                    >
                      <Option key="1" label="CSE">
                        CSE
                      </Option>
                    </Select>
                  </Form.Item>
                  <ReadOutlined className="custom-icons" />
                </div>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <div className="custom-icon-wrapper">
                  <Form.Item name="degree_type">
                    <Select
                      showSearch
                      className={
                        formValues.degree_type.length > 0
                          ? "university university-focus-within"
                          : "university"
                      }
                      size="large"
                      name="degree_type"
                      optionLabelProp="label"
                      bordered={false}
                      placeholder="Degree Type"
                    >
                      <Option key="10" key="Bachelor's ">
                        Bachelor's
                      </Option>
                      <Option key="20" key="Master's ">
                        Master's
                      </Option>
                      <Option key="30" key="Phd ">
                        Phd
                      </Option>
                      <Option key="40" key="Others">
                        Others
                      </Option>
                    </Select>
                  </Form.Item>
                  <BookOutlined className="custom-icons" />
                </div>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <div className="custom-icon-wrapper">
                  <Form.Item name="funding_semester">
                    <Select
                      showSearch
                      className={
                        formValues.funding_semester.length > 0
                          ? "university university-focus-within"
                          : "university"
                      }
                      size="large"
                      name="funding_semester"
                      optionLabelProp="label"
                      bordered={false}
                      placeholder="Semester"
                    >
                      <Option key="1" label="1st">
                        1st
                      </Option>
                      <Option key="2" label="2nd">
                        2nd
                      </Option>
                      <Option key="3" label="3rd">
                        3rd
                      </Option>
                      <Option key="4" label="4th">
                        4th
                      </Option>
                      <Option key="5" label="5th">
                        5th
                      </Option>
                      <Option key="6" label="6th">
                        6th
                      </Option>
                      <Option key="7" label="7th">
                        7th
                      </Option>
                      <Option key="8" label="8th">
                        8th
                      </Option>
                    </Select>
                  </Form.Item>
                  <WalletOutlined className="custom-icons" />
                </div>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <Form.Item name="funding_amount">
                  <Input
                    type="number"
                    placeholder="Funding Amount"
                    className={
                      formValues.funding_amount.length > 0
                        ? "university university-focus-within"
                        : "university"
                    }
                    name="funding_amount"
                    prefix={<DollarOutlined size="large" />}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <div className="custom-icon-wrapper">
                  <Form.Item
                    name="citizenship_status"
                    initialValue={
                      localStorage.getItem("citizenship_status")
                        ? localStorage.getItem("citizenship_status")
                        : null
                    }
                  >
                    <Select
                      showSearch
                      className={
                        formValues.citizenship_status.length > 0
                          ? "university university-focus-within"
                          : "university"
                      }
                      size="large"
                      optionLabelProp="label"
                      name="citizenship_status"
                      bordered={false}
                      placeholder="Citizenship Status"
                    >
                      <Option key="10" label="US citizen">
                        US citizen
                      </Option>

                      <Option key="30" label="Non-US citizen">
                        Non - US citizen
                      </Option>
                      <Option key="20" label="US Residen">
                        US Resident
                      </Option>
                    </Select>
                  </Form.Item>
                  <FlagOutlined className="custom-icons" />
                </div>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={12}>
                <div className="custom-icon-wrapper">
                  <Form.Item
                    name="funding_year"
                    initialValue={
                      localStorage.getItem("funding_year")
                        ? moment(localStorage.getItem("funding_year"))
                        : moment(undefined)
                    }
                  >
                    <DatePicker
                      suffixIcon={null}
                      picker="year"
                      name="funding_year"
                      placeholder="Funding Year"
                      className={
                        formValues.funding_year._isAMomentObject
                          ? "university date_wrapper university-focus-within"
                          : "university date_wrapper"
                      }
                    />
                  </Form.Item>
                  <CalendarOutlined className="custom-icons" />
                </div>
              </Col>
              <Col span={12}>
                <div className="custom-icon-wrapper">
                  <Form.Item
                    name="month"
                    initialValue={
                      localStorage.getItem("month")
                        ? moment(localStorage.getItem("month"))
                        : null
                    }
                  >
                    <DatePicker
                      suffixIcon={null}
                      picker="month"
                      placeholder="Month"
                      className={
                        formValues.month._isAMomentObject
                          ? "university date_wrapper university-focus-within"
                          : "university date_wrapper"
                      }
                      name="month"
                    />
                  </Form.Item>
                  <CalendarOutlined className="custom-icons" />
                </div>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col span={12}>
                <div className="custom-icon-wrapper">
                  <Form.Item name="grad_year">
                    <DatePicker
                      suffixIcon={null}
                      picker="year"
                      placeholder="Grad. Year"
                      className={
                        formValues.grad_year._isAMomentObject
                          ? "university date_wrapper university-focus-within"
                          : "university date_wrapper"
                      }
                      name="grad_year"
                    />
                  </Form.Item>
                  <CalendarOutlined className="custom-icons" />
                </div>
              </Col>
              <Col span={12}>
                <div className="custom-icon-wrapper">
                  <Form.Item name="grad_month">
                    <DatePicker
                      suffixIcon={null}
                      picker="month"
                      name="grad_month"
                      placeholder="Month"
                      className={
                        formValues.grad_month._isAMomentObject
                          ? "university date_wrapper university-focus-within"
                          : "university date_wrapper"
                      }
                    />
                  </Form.Item>
                  <CalendarOutlined className="custom-icons" />
                </div>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <Button
                  type="primary"
                  size="large"
                  className="save"
                  htmlType="submit"
                >
                  Submit
                </Button>
              </Col>
            </Row>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Create_ISA_S);
