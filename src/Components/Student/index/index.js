import { Layout } from "antd";
import React from "react";
import "./index.css";
import Head from "../Layout/Header/index";
import Foot from "../Layout/Footer/index";
import Right_Nav from "../Layout/Right-nav/index";
import image from "../../../Assets/images/nav-background-2.png";

const { Content } = Layout;

class Contents extends React.Component {
  state = {
    show_menu: false,
  };

  onClickMenu = () => {
    console.log("menu click");
    this.setState({
      show_menu: !this.state.show_menu,
    });
  };
  render() {
    return (
      <>
        <Head onClickMenu={this.onClickMenu} />
        <Content className="content-s">
          {this.state.show_menu ? (
            <div className="menu-background">
              <Right_Nav onClickMenu={this.onClickMenu} />
            </div>
          ) : (
            this.props.children
          )}
        </Content>
        {!this.state.show_menu && <Foot />}
        {this.state.show_menu && (
          <img
            src={image}
            style={{
              width: "100%",
              position: "fixed",
              bottom: "0",
              zIndex: "-1",
            }}
          />
        )}
      </>
    );
  }
}

export default Contents;
