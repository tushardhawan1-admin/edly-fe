import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  BankOutlined,
  BookOutlined,
  UnlockOutlined,
  FlagOutlined,
  ReadOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import {
  Button,
  Col,
  Collapse,
  DatePicker,
  Divider,
  Form,
  Image,
  Input,
  message,
  Row,
  Select,
  Steps,
} from "antd";

import "antd/dist/antd.css";
import moment from "moment";
import React, { Component } from "react";
import API_Manager from "../../../API/index";
import edit from "../../../Assets/images/edit-pencil.png";
import library_icon from "../../../Assets/images/library-active.png";
import mail_icon from "../../../Assets/images/mail-letter-active.png";
import unlock_icon from "../../../Assets/images/padlock-unlock-active.png";
import school_icon from "../../../Assets/images/school-active.png";
import profile_student from "../../../Assets/images/student-profile-image.png";
import user_icon from "../../../Assets/images/user-active.png";
import "./index.css";

const { Panel } = Collapse;
const { Option } = Select;

function callback(key) {
  console.log(key);
}

class Student_Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginContent: false,
      old_password: "",
      new_password: "",
      confirm_password: "",
    };
  }

  changePassword = (values) => {
    const formRef = React.createRef();
    API_Manager.studentChangePassword(localStorage.getItem("id"), values)
      .then((result) => {
        console.log("User changed password", result);
        this.setState({
          old_password: "",
          new_password: "",
          confirm_password: "",
        });
        // window.location.href="/student/landing"
      })
      .catch((error) => {
        console.log(error);
        message.error("You have entered wrong old password.");
      });
  };

  studentRetrieve = () => {
    API_Manager.studentDetail(localStorage.getItem("student_id"))
      .then((result) => {
        console.log(result);
        localStorage.setItem("date_of_birth", result.data.meta.date_of_birth);
        localStorage.setItem(
          "citizenship_status",
          result.data.data.citizenship_status
        );
        localStorage.setItem("address", result.data.meta.address);
        localStorage.setItem("pin_code", result.data.meta.pin_code.value);
        localStorage.setItem("school_name", result.data.data.school.name);
        localStorage.setItem("major", result.data.data.major);
        localStorage.setItem("current_year", result.data.data.current_year);
        localStorage.setItem(
          "completion_year",
          result.data.data.completion_year
        );
      })
      .catch((error) => console.log(error));
  };

  studentUpdate = (values) => {
    console.log(values);
    const dat = moment(values["date_of_birth"]).format("YYYY-MM-DD");
    const completion = moment(values["completion_year"]).format("YYYY");
    // const grad =moment(values[""])
    console.log(dat);
    values["date_of_birth"] = dat;
    values["completion_year"] = completion;
    values["user"] = localStorage.getItem("id");
    values["status"] = 10;
    console.log("values for post api", values);
    console.log(localStorage.getItem("student_id"));
    API_Manager.studentUpdate(localStorage.getItem("student_id"), values)
      .then((result) => {
        console.log("User Data Updated", result);

        message.success("Data updated Successfully.");
        this.studentRetrieve();

        // window.location.href="/student/landing"
      })
      .catch((error) => console.log(error));
  };
  showContent = () => {
    this.setState({
      loginContent: !this.state.loginContent,
    });
    console.log("clicked");
  };
  render() {
    const panelHeader1 = () => (
      <div className="panel-header" onLoad={() => this.studentRetrieve()}>
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={user_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">Personal Info</p>
      </div>
    );

    const panelHeader2 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={mail_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">Contact Info</p>
      </div>
    );

    const panelHeader3 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={school_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">Educational Info</p>
      </div>
    );

    const panelHeader4 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={library_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">ISA Applications</p>
      </div>
    );

    const panelHeader5 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={unlock_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">Change Password</p>
      </div>
    );
    return (
      <div
        className="desktop-profile-student"
        onLoad={(e) => this.studentRetrieve(e)}
      >
        <div className="profile-content">
          <Row>
            <Col span={24} className="student-profile-image">
              <Image
                src={profile_student}
                preview={false}
                className="profile-image"
              />
              <Image src={edit} preview={false} className="edit" />
            </Col>
          </Row>

          <Row>
            <Col span={24} className="student-name">
              <p>
                {localStorage.getItem("fname")}&nbsp;
                {localStorage.getItem("lname")}
              </p>
            </Col>
          </Row>

          <Row>
            <Col span={24} className="student-mail">
              <p>{localStorage.getItem("email")}</p>
            </Col>
          </Row>
          <div className="student-details">
            {/* <div className="status">
              <Row>
                <Col span={24}>
                  <Row>
                    <Col className="reg-no" span={19}>
                      #9103773457
                    </Col>
                    <Col className="application-status" span={5}>
                      <span className="red-dot">&#9679;</span>
                      <p>Pending</p>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24} className="student-amount">
                      $ 15,000
                    </Col>
                  </Row>

                  <Row>
                    <Col span={18} className="student-university">
                      University of Illinois at Urbana, Champagne
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div> */}

            <div className="student-details-form">
              <Collapse ghost expandIconPosition="right" className="collapse">
                {/* <div className="form-icon-background">
                      <Image
                        src={user_icon}
                        className="form-icon"
                        preview={false}
                      />
                    </div> */}
                <Panel header={panelHeader1()} key="1">
                  <div className="info">
                    <Form onFinish={this.studentUpdate}>
                      <Row gutter={8}>
                        <Col span={14}>
                          <Form.Item
                            name="first_name"
                            initialValue={localStorage.getItem("fname")}
                          >
                            <Input
                              type="text"
                              size="large"
                              className="first-input university"
                              placeholder="First Name"
                              name="fname"
                              prefix={
                                <UserOutlined
                                  size="large"
                                  className="mailico"
                                />
                              }
                            />
                          </Form.Item>
                        </Col>
                        <Col span={10}>
                          <Form.Item
                            name="last_name"
                            initialValue={localStorage.getItem("lname")}
                          >
                            <Input
                              type="text"
                              size="large"
                              name="lname"
                              className="first-input university"
                              placeholder="Last Name"
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name="date_of_birth"
                            initialValue={
                              localStorage.getItem("date_of_birth")
                                ? moment(localStorage.getItem("date_of_birth"))
                                : null
                            }
                          >
                            <DatePicker
                              name="date_of_birth"
                              className="first-input university"
                              format="DD-MM-YYYY"
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <div className="custom-icon-wrapper">
                            <Form.Item
                              name="citizenship_status"
                              initialValue={
                                localStorage.getItem("citizenship_status")
                                  ? localStorage.getItem("citizenship_status")
                                  : null
                              }
                            >
                              <Select
                                showSearch
                                className="first-input university"
                                size="large"
                                optionLabelProp="label"
                                name="citizenship_status"
                                bordered={false}
                                placeholder="Citizenship Status"
                              >
                                <Option key="10" label="US citizen">
                                  US citizen
                                </Option>

                                <Option key="30" label="Non-US citizen">
                                  Non - US citizen
                                </Option>
                                <Option key="20" label="US Resident">
                                  US Resident
                                </Option>
                              </Select>
                            </Form.Item>
                            <FlagOutlined className="firstintput custom-icons" />
                          </div>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Button
                            type="primary"
                            size="large"
                            className="save"
                            htmlType="submit"
                          >
                            Save
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </Panel>
                <Divider />
                <Panel header={panelHeader2()} key="2">
                  <div className="info">
                    <Form onFinish={this.studentUpdate}>
                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name="email"
                            initialValue={
                              localStorage.getItem("email")
                                ? localStorage.getItem("email")
                                : null
                            }
                          >
                            <Input
                              type="email"
                              size="large"
                              name="email"
                              className="first-input university"
                              placeholder="E-mail"
                              prefix={<MailOutlined size="large" />}
                            />
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row>
                        {/* <Col span={8}>
                          <Form.Item
                            name="country_code"
                            initialValue={
                              localStorage.getItem("country_code")
                                ? localStorage.getItem("country_code")
                                : null
                            }
                          >
                            <Select
                              size="large"
                              name="country_code"
                              bordered={false}
                              defaultActiveFirstOption
                              placeholder="+01"
                              className="first-input"
                            >
                              <Option key="+01">+01</Option>
                              <Option key="+91">+91</Option>
                            </Select>
                          </Form.Item>
                        </Col> */}

                        <Col span={24}>
                          <Form.Item
                            name="mobile"
                            initialValue={
                              localStorage.getItem("mobile")
                                ? localStorage.getItem("mobile")
                                : null
                            }
                          >
                            <Input
                              type="number"
                              className="first-input university"
                              size="large"
                              name="mobile"
                              placeholder="Phone Number"
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name="address"
                            initialValue={
                              localStorage.getItem("address")
                                ? localStorage.getItem("address")
                                : null
                            }
                          >
                            <Input.TextArea
                              size="large"
                              name="address"
                              className="first-input university"
                              placeholder="Address"
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name="pin_code"
                            initialValue={
                              localStorage.getItem("pin_code")
                                ? localStorage.getItem("pin_code")
                                : null
                            }
                          >
                            <Select
                              className="first-input university"
                              showSearch
                              size="large"
                              name="pin_code"
                              bordered={false}
                              placeholder="Postal Code"
                            >
                              <Option key="122015" label="122015">
                                122015
                              </Option>
                              <Option key="342220" label="342220">
                                342220
                              </Option>
                            </Select>
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Button
                            type="primary"
                            size="large"
                            className="save"
                            htmlType="submit"
                          >
                            Save
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </Panel>
                <Divider />
                <Panel header={panelHeader3()} key="3">
                  <div className="info">
                    <Form onFinish={this.studentUpdate}>
                      <Row>
                        <Col span={24}>
                          <div className="custom-icon-wrapper">
                            <Form.Item
                              name="school_name"
                              initialValue={
                                localStorage.getItem("school_name")
                                  ? localStorage.getItem("school_name")
                                  : null
                              }
                            >
                              <Select
                                showSearch
                                className="first-input university"
                                size="large"
                                bordered={false}
                                optionLabelProp="label"
                                name="school_name"
                                placeholder="Name of Institute"
                              >
                                <Option
                                  key="University of USA"
                                  label="University of USA"
                                >
                                  University of USA
                                </Option>
                              </Select>
                            </Form.Item>
                            <BankOutlined className="firstinput custom-icons" />
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={24}>
                          <div className="custom-icon-wrapper">
                            <Form.Item
                              name="major"
                              initialValue={
                                localStorage.getItem("major")
                                  ? localStorage.getItem("major")
                                  : null
                              }
                            >
                              <Select
                                showSearch
                                className="first-input university"
                                size="large"
                                name="major"
                                optionLabelProp="label"
                                bordered={false}
                                placeholder="Major Field"
                              >
                                <Option key="1" label="CSE">
                                  CSE
                                </Option>
                              </Select>
                            </Form.Item>
                            <ReadOutlined className="firstinput custom-icons" />
                          </div>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <div className="custom-icon-wrapper">
                            <Form.Item
                              name="current_year"
                              initialValue={
                                localStorage.getItem("current_year")
                                  ? localStorage.getItem("current_year")
                                  : null
                              }
                            >
                              <Select
                                className="first-input university"
                                size="large"
                                name="current_year"
                                optionLabelProp="label"
                                bordered={false}
                                placeholder="Year"
                              >
                                <Option key="1" label="1st Year">
                                  1st Year
                                </Option>
                                <Option key="2" label="2nd Year">
                                  2nd Year
                                </Option>
                                <Option key="3" label="3rd Year">
                                  3rd Year
                                </Option>
                                <Option key="4" label="4th Year">
                                  4th Year
                                </Option>
                                <Option key="5" label="5th Year">
                                  5th Year
                                </Option>
                                <Option key="6" label="6th Year">
                                  6th Year
                                </Option>
                                <Option key="7" label="7th Year">
                                  7th Year
                                </Option>
                                <Option key="8" label="8th Year">
                                  8th Year
                                </Option>
                              </Select>
                            </Form.Item>
                            <CalendarOutlined className="firstinput custom-icons" />
                          </div>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name="completion_year"
                            initialValue={
                              localStorage.getItem("completion_year")
                                ? moment(
                                    localStorage.getItem("completion_year")
                                  )
                                : null
                            }
                          >
                            <DatePicker
                              picker="year"
                              placeholder="Year of Completion"
                              className="first-input university"
                              name="completion_year"
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Button
                            type="primary"
                            size="large"
                            className="save"
                            htmlType="submit"
                          >
                            Save
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </Panel>
                {/* <Divider />
                <Panel header={panelHeader4()} key="4">
                  Hi
                </Panel> */}
                <Divider />
                <Panel header={panelHeader5()} key="5">
                  <div className="info">
                    <Form onFinish={this.changePassword}>
                      <Row>
                        <Col span={24}>
                          <Form.Item name="old_password">
                            <Input.Password
                              size="large"
                              className="first-input university"
                              placeholder="Old Password"
                              name="old_password"
                              prefix={<UnlockOutlined size="large" />}
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name="new_password"
                            hasFeedback
                            rules={[
                              {
                                required: true,
                                message: "Please enter password",
                              },
                              {
                                min: 8,
                                message: "Please enter at least 8 charecters",
                              },
                            ]}
                          >
                            <Input.Password
                              size="large"
                              className="first-input university"
                              placeholder="New Password"
                              name="new_password"
                              prefix={<UnlockOutlined size="large" />}
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name="confirm_password"
                            hasFeedback
                            rules={[
                              {
                                required: true,
                                message: "Please confirm your password!",
                              },
                              ({ getFieldValue }) => ({
                                validator(_, value) {
                                  if (
                                    !value ||
                                    getFieldValue("new_password") === value
                                  ) {
                                    return Promise.resolve();
                                  }

                                  return Promise.reject(
                                    "The two passwords that you entered do not match!"
                                  );
                                },
                              }),
                            ]}
                            dependencies={["new_password"]}
                          >
                            <Input.Password
                              size="large"
                              className="first-input university"
                              placeholder="Confirm Password"
                              name="confirm_password"
                              prefix={<UnlockOutlined size="large" />}
                            />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={24}>
                          <Button
                            type="primary"
                            size="large"
                            htmlType="submit"
                            className="save"
                          >
                            Send
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </div>
                </Panel>
              </Collapse>
              {/* <button onClick={this.showContent}>jjjjjjjjjjjjjjjj</button>
                {this.state.loginContent && <div>hie</div>} */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Student_Profile;
