import { MailOutlined } from "@ant-design/icons";
import { Button, Col, Form, Image, Input, Row } from "antd";
import "antd/dist/antd.css";
import React from "react";
import { Link, withRouter } from "react-router-dom";
import API_Manager from "../../../API/index";
import logo from "../../../Assets/images/Logo/Logo.svg";
import go_back from "../../../Assets/images/send-forward.png";
import "./index.css";

const Forgot_Password = (props) => {
  const forgotPassword = (values) => {
    console.log("emailo", values);
    API_Manager.forgotPassword(values)
      .then((result) => {
        console.log("Email sent", result);
        const res = result.data.status;
        console.log(res);
        props.history.props.push("/student/forgot-password-1");
      })
      .catch((error) => console.log(error));
  };
  return (
    <div className="forgot-password-desktop">
      <div className="go-back">
        <Button type="text" onClick={props.history.goBack}>
          <Image src={go_back} preview={false} />
          <span className="back-go"> Go Back</span>
        </Button>
      </div>
      <div className="forgot-password-background">
        <Col className="view">
          <Col className="logo-signup" span={24}>
            <img src={logo} className="logomobile" alt="img"></img>
          </Col>
          <Row>
            <Col className="welcome-signup" span={24}>
              Forgot Password
            </Col>
          </Row>

          <Row justify="center">
            <Col className="create-your">{/* Create your edly Account */}</Col>
          </Row>

          <Form className="password-form" onFinish={forgotPassword}>
            <Row>
              <Col span={24}>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      type: "email",
                      message: "The input is not valid E-mail!",
                    },
                    {
                      required: true,
                      message: "Please input your E-mail!",
                    },
                  ]}
                >
                  <Input
                    type="email"
                    size="large"
                    className="email-input-password university"
                    placeholder="E-mail"
                    name="email"
                    prefix={<MailOutlined size="large" className="mailico" />}
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row justify="center">
              <Col className="submit-password" span={24}>
                <Button
                  type="primary"
                  className="submit-button-password"
                  size="large"
                  htmlType="submit"
                >
                  Submit
                </Button>
              </Col>
            </Row>
          </Form>

          <div style={{height:'30px'}}></div>

          {/* <Row justify="center">
            <Col className="login-now" span={24}>
              <Link>Login</Link> now
            </Col>
          </Row> */}
        </Col>
      </div>
    </div>
  );
};

export default withRouter(Forgot_Password);
