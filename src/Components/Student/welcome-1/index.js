import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useCallback, useState } from "react";
import logo from "../../../Assets/images/Logo/Logo.svg";
import { UserOutlined, MailOutlined, PhoneOutlined } from "@ant-design/icons";
import go_back from "../../../Assets/images/send-forward.png";
import axios from "axios";
import API_MANAGER from "../../../API/index";

import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Checkbox,
  Button,
  Image,
  Input,
  Form,
  Select,
} from "antd";
import { Link, useHistory } from "react-router-dom";

const { Content } = Layout;
const { Option } = Select;

function WelcomeMobile({ history }) {
  return (
    <div className="signup-desktop1">
      <div className="go-back">
      <Button type="text"  onClick={history.goBack}>
            <Image src={go_back} preview={false} />
           <span className="back-go"> Go Back</span>
          </Button>
      </div>
      <div className="signupBackground">
        <Col className="view">
          <Col className="logo-signup-1" span={24}>
            <img src={logo} className="logomobile"></img>
          </Col>
          {/* <Row>
              <Col className="welcome-signup" span={24}>
                Welcome
              </Col>
            </Row> */}

          {/* <Row justify="center">
              <Col className="create-your">Create your edly Account</Col>
            </Row> */}

          <Row justify="center">
            <Col span={24}>
              <p className="welcome-1-text">
                <p>
                It looks like you are not signed in to the your Edly students
                account.</p><p> We are working on processing your request, In the mean
                time login to your account, or create one if you don't have one
                to continue.</p><p> It wont take long. We promise.</p>
              </p>
            </Col>
          </Row>

          <Row justify="center">
            <Col className="signup-welcome1" span={24}>
              <Link to="/student/signup">
                <Button type="default" className="signup-button-welcome1" size="large">
                  Sign Up
                </Button>
              </Link>
            </Col>
          </Row>

          <Row>
            <Col span={24}>
              <div className="or">Or</div>
            </Col>
          </Row>

          <Row justify="center">
            <Col span={24}>
              <div className="already-have-account">
                Already have and account ?{" "}
                <Link to="/student/login">Login</Link> now.
              </div>
            </Col>
          </Row>
        </Col>
      </div>
    </div>
  );
}
export default WelcomeMobile;
