import {
  BankOutlined,
  BookOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import {
  Button,
  Col,
  DatePicker,
  Form,
  Image,
  Layout,
  Row,
  Select,
  Steps,
} from "antd";
import "antd/dist/antd.css";
import React, { Component } from "react";
import dollar_bag from "../../../Assets/images/bag-dollar.png";
import journal from "../../../Assets/images/journal-pencil.svg";
import landing_image2 from "../../../Assets/images/landing-image-2.png";
import landing_image3 from "../../../Assets/images/landing-image-3.png";
import landing_image from "../../../Assets/images/landing-image.png";
import stocks_up from "../../../Assets/images/stocks-up.png";
import user_heart from "../../../Assets/images/user-heart.png";
import Empty_circle from "../table-symbols/empty_circle/index";
import Filled_circle from "../table-symbols/filled_circle/index";
import "./index.css";

const { Content } = Layout;
const { Option } = Select;
const { Step } = Steps;

class Landing_Page extends Component {
  isaForm = React.createRef();
  state = {
    current: 0,
    form_values: {
      school_name: "",
      major: "",
      current_year: "",
      funding_year: "",
      month: "",
    },
  };

  onFormChange = (values, all_values) => {
    console.log(
      "values and all values",
      values,
      all_values,
      this.state.form_values
    );
    const key = Object.keys(values)[0];
    const new_form = this.state.form_values;
    new_form[key] = values[key];
    this.setState({ form_values: new_form });
    console.log("state values:", this.state.form_values, key, new_form);
  };

  checkNow = (values) => {
    console.log("values:", values);
    const school_name = values["school_name"];
    localStorage.setItem("school_name", school_name);
    const major = values["major"];
    localStorage.setItem("major", major);
    const current_year = values["current_year"];
    localStorage.setItem("current_year", current_year);
    const funding_year = values["funding_year"];
    localStorage.setItem("funding_year", funding_year);
    const month = values["month"];
    localStorage.setItem("month", month);
    this.props.history.push("/student/create-isa");
  };

  onChange = (current) => {
    console.log("onChange:", current);
    this.setState({ current });
  };

  render() {
    const { current } = this.state;
    const desc_title = [
      "Apply",
      "Get Approved",
      "Receive Funding",
      "Go to school",
      "Get a Job",
      "Never Stress",
    ];
    const descinfo = [
      "Apply to see if you're eligible in 1 minute or less and don't worry, your application information will never effect your credit score.",
      "Edly processes applications and returns students with ISA contract terms. Once you're approved, Edly sets you up with your account servicer.",
      "Edly Sends directly to your school. You don't need to lift a finger.",
      "Keep your head down and your grades up! Focus on your Education without ever paying a dime.",
      "Once you land a job making above a defined minimum (Usually $30-40K per year) You'll Start paying a percentage of your income back to Edly.",
      "If life throws lemons, Your ISA payments are put on hold.",
    ];
    const desc = (i) => (
      <>
        <Image src={journal} style={{ fontSize: "20px" }} />
        <p>{descinfo[i]}</p>
      </>
    );
    const form_values = this.state.form_values;
    return (
      <div className="desktop-landing">
        <div className="main-content">
          <Row>
            <Col xs={24} md={12} lg={12}>
              <Image
                src={landing_image}
                preview={false}
                className="landing-image"
              />
            </Col>
            <Col xs={24} md={12} lg={12}>
              <Row>
                <Col xs={24} className="focus-column">
                  <p className="focus">
                    Focus On your education, Not on your tuition.
                  </p>
                </Col>
                <Col xs={24} className="check-button">
                  <Button
                    type="primary"
                    size="large"
                    className="check-now"
                    href="/student/create-isa/"
                  >
                    Check Now
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col xs={24}>
              <Row>
                <Col span={24} className="what-is-isa">
                  <div className="isa"> What is ISA</div>
                </Col>
                <Col span={24} className="what-is-isa-desc">
                  <div className="isa-desc">
                    Instead of loans, edly offers ISAs – or Income Share
                    Agreements. With ISAs, you only pay for your tuition once
                    you land your first well-paying job. Payments adjust with
                    your income and are designed to be affordable.
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
          <div className="features">
            <Row>
              <Col span={24} className="title">
                <div className="feat">Features of an Edly ISA</div>
                <Row gutter={{ xs: 8, sm: 16, md: 32, lg: 32 }}>
                  <Col
                    xs={24}
                    md={8}
                    lg={8}
                    className="image"
                    style={{ marginTop: "30px" }}
                  >
                    <Image
                      src={dollar_bag}
                      preview={false}
                      className="image-icon"
                    />
                    <div className="feature-name">learn now, pay later</div>
                    <div className="feature-desc">
                      School Comes First. Don't pay a cent until you land your
                      first well-paying job.
                    </div>
                  </Col>
                  <Col xs={24} md={8} lg={8} className="image">
                    <Image
                      src={user_heart}
                      preview={false}
                      className="image-icon"
                    />
                    <div className="feature-name">Tailored, to you</div>
                    <div className="feature-desc">
                      Payment amounts adjust with your salary. Lose your job?
                      You don't owe a thing *
                    </div>
                  </Col>
                  <Col xs={24} md={8} lg={8} className="image">
                    <Image
                      src={stocks_up}
                      preview={false}
                      className="image-icon"
                    />
                    <div className="feature-name">Invested In Your success</div>
                    <div className="feature-desc">
                      We partner with accredited investors who are
                      professionally approved to lend to students.
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>

          <Row justify="center">
            <Col xs={24} md={10} lg={10}>
              <div className="title">How it works</div>
              <div className="steps">
                <Steps
                  current={current}
                  direction="vertical"
                  onChange={this.onChange}
                  // onMouseEnter={this.onChange}
                  // onMouseEnter={this.onChange}
                  // onMouseLeave={}
                  trigger="hover"
                >
                  {descinfo.map((item, i) => (
                    <Step
                      status={this.state.current === i ? "process" : "wait"}
                      title={
                        this.state.current === i ? (
                          <p className="step">{desc_title[i]}</p>
                        ) : (
                          <p className="step">{desc_title[i]}</p>
                        )
                      }
                      description={
                        this.state.current === i ? (
                          <p className="step-desc">{desc(i)}</p>
                        ) : (
                          <h1></h1>
                        )
                      }
                    />
                  ))}
                </Steps>
              </div>
            </Col>
          </Row>
          <Row justify="center">
            <Col xs={24} md={10} lg={10}>
              <div className="title">Check your ISA eligibility</div>
              <div className="isa-form">
                <Form
                  ref={this.isaForm}
                  onFinish={this.checkNow}
                  name="isaForm"
                  onValuesChange={this.onFormChange}
                >
                  <div className="custom-icon-wrapper">
                    <Form.Item
                      name="school_name"
                      initialValue={
                        localStorage.getItem("school_name")
                          ? localStorage.getItem("school_name")
                          : null
                      }
                    >
                      <Select
                        showSearch
                        className={
                          form_values.school_name.length > 0
                            ? "university university-focus-within"
                            : "university"
                        }
                        size="large"
                        optionLabelProp="label"
                        name="school_name"
                        bordered={false}
                        placeholder="Name of Institute"
                      >
                        <Option
                          key="University of USA"
                          label="University of USA"
                        >
                          University of USA
                        </Option>
                      </Select>
                    </Form.Item>
                    <BankOutlined
                      className={
                        form_values.school_name.length > 0
                          ? "custom-icons custom-icons-active"
                          : "custom-icons"
                      }
                    />
                  </div>
                  <Row>
                    <Col xs={24}>
                      <div className="custom-icon-wrapper">
                        <Form.Item
                          name="major"
                          initialValue={
                            localStorage.getItem("major")
                              ? localStorage.getItem("major")
                              : null
                          }
                        >
                          <Select
                            showSearch
                            className={
                              form_values.major.length > 0
                                ? "university university-focus-within"
                                : "university"
                            }
                            size="large"
                            optionLabelProp="label"
                            bordered={false}
                            name="major"
                            placeholder="Major Field"
                          >
                            <Option key="1" label="CSE">
                              CSE
                            </Option>
                          </Select>
                        </Form.Item>
                        <BookOutlined
                          className={
                            form_values.major.length > 0
                              ? "custom-icons custom-icons-active"
                              : "custom-icons"
                          }
                        />
                      </div>
                    </Col>
                    <Col xs={24}>
                      <div className="custom-icon-wrapper">
                        <Form.Item
                          name="current_year"
                          initialValue={
                            localStorage.getItem("current_year")
                              ? localStorage.getItem("current_year")
                              : null
                          }
                        >
                          <Select
                            className="first-input university"
                            size="large"
                            name="current_year"
                            optionLabelProp="label"
                            bordered={false}
                            placeholder="Year"
                          >
                            <Option key="1" label="1st Year">
                              1st Year
                            </Option>
                            <Option key="2" label="2nd Year">
                              2nd Year
                            </Option>
                            <Option key="3" label="3rd Year">
                              3rd Year
                            </Option>
                            <Option key="4" label="4th Year">
                              4th Year
                            </Option>
                            <Option key="5" label="5th Year">
                              5th Year
                            </Option>
                            <Option key="6" label="6th Year">
                              6th Year
                            </Option>
                            <Option key="7" label="7th Year">
                              7th Year
                            </Option>
                            <Option key="8" label="8th Year">
                              8th Year
                            </Option>
                          </Select>
                        </Form.Item>
                        {/* <CalendarOutlined className="firstinput custom-icons" /> */}
                        <CalendarOutlined
                          className={
                            form_values.current_year._isAMomentObject
                              ? "custom-icons custom-icons-active"
                              : "custom-icons"
                          }
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row gutter={16}>
                    <Col span={13}>
                      <div className="custom-icon-wrapper">
                        <Form.Item name="funding_year">
                          <DatePicker
                            picker="year"
                            placeholder="Funding Year"
                            className={
                              form_values.funding_year._isAMomentObject
                                ? "university date_wrapper university-focus-within"
                                : "university date_wrapper"
                            }
                            name="funding_year"
                            suffixIcon={null}
                          />
                        </Form.Item>
                        <CalendarOutlined
                          className={
                            form_values.funding_year._isAMomentObject
                              ? "custom-icons custom-icons-active"
                              : "custom-icons"
                          }
                        />
                      </div>
                    </Col>
                    <Col span={11}>
                      <div className="custom-icon-wrapper">
                        <Form.Item name="month">
                          <DatePicker
                            picker="month"
                            placeholder="Month"
                            className={
                              form_values.month._isAMomentObject
                                ? "university date_wrapper university-focus-within"
                                : "university date_wrapper"
                            }
                            mode="month"
                            format="MMMM"
                            name="month"
                            suffixIcon={null}
                          />
                        </Form.Item>
                        <CalendarOutlined
                          className={
                            form_values.month._isAMomentObject
                              ? "custom-icons custom-icons-active"
                              : "custom-icons"
                          }
                        />
                      </div>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24}>
                      <Button
                        type="primary"
                        size="large"
                        className="check-now"
                        htmlType="submit"
                      >
                        Check Now
                      </Button>
                    </Col>
                  </Row>
                </Form>
              </div>
            </Col>
          </Row>

          <Row>
            <Col span={24}>
              <Image
                src={landing_image2}
                preview={false}
                className="landing-image"
              />
            </Col>
          </Row>
          <div className="why-is-isa">
            <Row>
              <Col
                span={24}
                className="title"
                style={{ marginTop: "30px", marginBottom: "20px" }}
              >
                <div className="feat">Why Is an ISA right for me?</div>
              </Col>
            </Row>

            <Row>
              <Col span={24} className="why-is-isa-desc">
                <div className="isa-desc">
                  Many existing education financing options create an unfair
                  burden for student. ISAs ensure that if you do not succeed up
                  to a certain level your payment amount is less or zero.
                </div>
              </Col>
            </Row>

            <div className="table">
              <Row className="table-title">
                <Col span={13} className="name">
                  <p className="title">Features</p>
                </Col>
                <Col span={5} className="edly">
                  <p className="title">Edly</p>
                </Col>
                <Col span={6} className="private-loans">
                  <p className="title">Private Loans</p>
                </Col>
              </Row>

              <Row>
                <Col span={13} className="name">
                  <p>High FICO score</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={6} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Co-signer</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={6} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Approval rate</p>
                </Col>
                <Col span={5} className="edly">
                  High
                </Col>
                <Col span={6} className="private-loans">
                  Low
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Interest rate</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={6} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>income-based repayment</p>
                </Col>
                <Col span={5} className="edly">
                  <Filled_circle color="#ffffff" />
                </Col>
                <Col span={6} className="private-loans">
                  <Empty_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Number of payments</p>
                </Col>
                <Col span={5} className="edly">
                  Fixed
                </Col>
                <Col span={6} className="private-loans">
                  Indefinite
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Maximum repayment amount</p>
                </Col>
                <Col span={5} className="edly">
                  Fixed
                </Col>
                <Col span={6} className="private-loans">
                  Indefinite
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Debt</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={6} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Minimum income threshold</p>
                </Col>
                <Col span={5} className="edly">
                  <Filled_circle color="#ffffff" />
                </Col>
                <Col span={6} className="private-loans">
                  <Empty_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Employment guarantee</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={6} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Auto-pay student discount</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={6} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={13} className="name">
                  <p>Value signal/recruiting effectiveness</p>
                </Col>
                <Col span={5} className="edly">
                  High
                </Col>
                <Col span={5} className="private-loans last">
                  None
                </Col>
              </Row>
            </div>
          </div>

          <Row>
            <Col span={24} className="image-3">
              <Image
                src={landing_image3}
                preview={false}
                className="landing-image-3"
              />
            </Col>
          </Row>

          <Row>
            <Col span={24} className="check-button">
              <Button
                type="primary"
                size="large"
                className="check-now"
                href="/student/create-isa/"
                style={{ marginTop: "0" }}
              >
                Check Now
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Landing_Page;
