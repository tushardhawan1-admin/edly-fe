import "antd/dist/antd.css";
import "./index.css";
import {
  BankOutlined,
  DownOutlined,
  BookOutlined,
  CalendarOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
  Select,
  Steps,
} from "antd";
import blog_image1 from "../../../Assets/images/blog-image1.png";
import blog_image2 from "../../../Assets/images/blog-image2.png";
import journal from "../../../Assets/images/journal-pencil.svg";

import media1 from "../../../Assets/images/media1.png";

const { Content } = Layout;
const { Option } = Select;
const { Step } = Steps;

class Landing_Page extends Component {
  isaForm = React.createRef();
  state = {
    current: 0,
  };

  onChange = (current) => {
    console.log("onChange:", current);
    this.setState({ current });
  };

  render() {
    const { current } = this.state;
    const desc_title = [
      "Apply",
      "Get Approved",
      "Receive Funding",
      "Go to school",
      "Get a Job",
      "Never Stress",
    ];
    const descinfo = [
      "Apply to see if you're eligible in 1 minute or less and don't worry, your application information will never effect your credit score.",
      "Edly processes applications and returns students with ISA contract terms. Once you're approved, Edly sets you up with your account servicer.",
      "Edly Sends directly to your school. You don't need to lift a finger.",
      "Keep your head down and your grades up! Focus on your Education without ever paying a dime.",
      "Once you land a job making above a defined minimum (Usually $30-40K per year) You'll Start paying a percentage of your income back to Edly.",
      "If life throws lemons, Your ISA payments are put on hold.",
    ];
    const desc = (i) => (
      <>
        <Image src={journal} style={{ fontSize: "20px" }} />
        <p>{descinfo[i]}</p>
      </>
    );

    return (
      <div className="desktop-landing-1">
        <div className="main-content">
          <Row>
            <Col xs={24} md={12} lg={12}>
              <Image src={media1} preview={false} className="landing-image" />
            </Col>
          </Row>
          <div className="resource-media-details-content">
            <Row>
              <Col span={24} className="title-resources">
                How to fund your education smartly?
              </Col>
            </Row>
            <Row>
              <Col className="edly-admin" span={12}>
                Edly admi<p className="date-20">Jan 20</p>
              </Col>
              <Col className="read-2" span={12}>
                Read: 2 mins
              </Col>
            </Row>

            <Row>
              <Col className="content-edly-admin" span={24}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Dolor purus non enim praesent elementum facilisis leo vel. Donec
                ac odio tempor orci dapibus ultrices in. Nascetur ridiculus mus
                mauris vitae ultricies. Orci ac auctor augue mauris augue neque
                gravida in fermentum. Non tellus orci ac auctor augue. Et odio
                pellentesque diam volutpat commodo sed. Vitae elementum
                curabitur vitae nunc sed velit dignissim sodales ut. Sed enim ut
                sem viverra aliquet eget sit amet tellus. Risus nec feugiat in
                fermentum posuere. Vestibulum lorem sed risus ultricies
                tristique nulla aliquet enim tortor. Diam maecenas ultricies mi
                eget mauris pharetra et. Odio tempor orci dapibus ultrices in.
                Praesent elementum facilisis leo vel fringilla. Hendrerit
                gravida rutrum quisque non tellus orci ac auctor augue. Sed
                ullamcorper morbi tincidunt ornare. Maecenas pharetra convallis
                posuere morbi leo urna molestie. Nulla aliquet porttitor lacus
                luctus accumsan tortor posuere ac. Porttitor eget dolor morbi
                non arcu risus quis varius. Pellentesque nec nam aliquam sem et.
                At varius vel pharetra vel turpis nunc eget. Nunc vel risus
                commodo viverra maecenas accumsan lacus. Vitae et leo duis ut
                diam quam nulla porttitor. Neque aliquam vestibulum morbi
                blandit cursus risus at ultrices mi.
              </Col>
            </Row>
          </div>
          <Row>
            <Col span={24} className="title">
              Related Blogs
            </Col>
          </Row>
          <div className="footer-links">
            <Row gutter={32}>
              <Col span={12}>
                <Image src={blog_image1} preview={false} />
                <a className="resource-media-details-title-1">
                  Stats and evidences: popularity of … ISA.
                </a>
              </Col>

              <Col span={12}>
                <Image src={blog_image2} preview={false} />
                <a className="resource-media-details-title-1">
                  Investment on Students : changing world
                </a>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}
export default Landing_Page;
