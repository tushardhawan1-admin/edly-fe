import { Button, Col, Image, Layout, Row } from "antd";
import "antd/dist/antd.css";
import React from "react";
import { Link } from "react-router-dom";
import logo from "../../../Assets/images/Logo/Logo.svg";
import go_back from "../../../Assets/images/send-forward.png";
import "./index.css";


const WelcomeMobile = (props) => {
  return (
    <div className="forgot-password-desktop">
      <div className="go-back">
        <Button type="text" onClick={props.history.goBack}>
          <Image src={go_back} preview={false} />
          <span className="back-go"> Go Back</span>
        </Button>
      </div>
      <div className="forgot-password-background">
        <Col className="view">
          <Col className="logo-signup" span={24}>
            <img src={logo} className="logomobile" alt="img"></img>
          </Col>
          <Row>
            <Col className="welcome-signup" span={24}>
              Forgot Password
            </Col>
          </Row>

          <Row justify="center">
            <Col className="create-your">{/* Create your edly Account */}</Col>
          </Row>

          <Row justify="center">
            <Col span={24}>
              <p className="forgot-password-text">
                A link with instructions to reset your password has been sent to
                the registered email id.
              </p>
              <p className="forgot-password-text">
                Follow the instruction on the link to reset the password
              </p>
            </Col>
          </Row>

          {/* <Form className="password-form">
              <Row>
                <Col span={24}>
                  <Form.Item>
                    <Input
                      type="email"
                      size="large"
                      className="email-input-password"
                      placeholder="E-mail"
                      prefix={<MailOutlined size="large" className="mailico" />}
                    />
                  </Form.Item>
                </Col>
              </Row>

              <Row justify="center">
                <Col className="submit-password" span={24}>
                  <Button
                    type="primary"
                    className="submit-button-password"
                    size="large"
                  >
                    Submit
                  </Button>
                </Col>
              </Row>
            </Form> */}

          <Row justify="center">
            <Col className="login-now" span={24}>
              <Link className="fp-login">Login</Link> now
            </Col>
          </Row>
        </Col>
      </div>
    </div>
  );
};
export default WelcomeMobile;
