import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import { Link, Route } from "react-router-dom";
import logo from "../../../Assets/images/Logo/Logo.png";
import go_back from "../../../Assets/images/send-forward.png";
import not_eligible from "../../../Assets/images/not-eligible.png";

import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
} from "antd";

const { Content } = Layout;

const Isa_eligibility_no=(props)=> {
  return (
    <div className="desktop-landing">
      <div className="go-back-no">
      <Button type="text"  onClick={props.history.goBack}>
            <Image src={go_back} preview={false} />
           <span className="back-go"> Go Back</span>
          </Button>
      </div>
      <div className="main-content">
        <Row>
          <Col span={24} className="our-partners">
            Our Partners are hear to help
          </Col>
        </Row>
        <Row>
          <Col span={24} className="we-are-sorry">
            We're sorry - unfortunately, based on the information you provided,
            Edly is unable to fund your ISA at this time. We look at many
            different factors to determine eligibility, including the following
            criteria :
          </Col>
        </Row>

        <Row>
          <Col span={24} className="us-citizen">
            <p>US Citizen or Permanent Resident</p><p> Attending graduate and post
            bachelors programs, or final year of study in an undergraduate
            program</p><p> Program graduation rate</p><p> We're always expanding access, so
            please come back next semester!</p><p> Still looking for funding? We've
            partnered with mission-driven private lenders that may be able to
            help if you need additional funding.</p>
          </Col>
        </Row>

        <Row>
          <Col span={24} className="not-eligible">
            <Image src={not_eligible} preview={false}/>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Isa_eligibility_no;
