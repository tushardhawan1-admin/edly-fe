import React from 'react';

function Big_blue_circle(props) {
    return (
        <div className="big-blue-circle">
            {props.children}
        </div>
    );
}

export default Big_blue_circle;
