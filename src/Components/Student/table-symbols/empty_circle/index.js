import "antd/dist/antd.css";
import "./index.css";
import {
  BankOutlined,
  DownOutlined,
  BookOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
} from "antd";

const { Content } = Layout;

const Empty_Circle =(props)=> {
  
    return (
        <div className="empty_circle" style={{borderColor:props.color}}>

        </div>
      
    );
  }
export default Empty_Circle;