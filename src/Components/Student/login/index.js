import { MailOutlined, UnlockOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";
import React from "react";
import API_Manager from "../../../API/index";
import logo from "../../../Assets/images/Logo/Logo.svg";
import go_back from "../../../Assets/images/send-forward.png";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  message,
} from "antd";
import { Link, useHistory, withRouter } from "react-router-dom";
import axios from "axios";
import "./index.css";

const { Content } = Layout;

const WelcomeStudent = (props) => {
  const loginStudent = (values) => {
    API_Manager.login(values)
      .then((result) => {
        console.log("User Logged In", result);
        const res = result.data.data.access_token;
        const id = result.data.meta.id;
        const fname = result.data.meta.first_name;
        const lname = result.data.meta.last_name;
        const email = result.data.meta.email;
        const mobile = result.data.meta.profile.mobile;
        // const school = result.data.data.student.school.name;
        const student_id = result.data.meta.user_type.id;
        console.log(res);
        console.log(id);
        localStorage.setItem("Token", res);
        localStorage.setItem("id", id);
        localStorage.setItem("fname", fname);
        localStorage.setItem("lname", lname);
        localStorage.setItem("email", email);
        localStorage.setItem("mobile", mobile);
        localStorage.setItem("student_id", student_id);
        // localStorage.setItem("school",school);

        console.log(localStorage.getItem("Token"));
        console.log(localStorage.getItem("id"));
        // window.location.href="/student/landing"
        props.history.push("/student/");
      })
      .catch((error) => {
        console.log(error);
        message.error("Email and password are incorrect.");
      });
  };

  return (
    <div className="login-desktop">
      <div className="go-back">
        <Button type="text" onClick={props.history.goBack}>
          <Image src={go_back} preview={false} />
          <span className="back-go"> Go Back</span>
        </Button>
      </div>
      <div className="mobile-background">
        <Col className="view">
          <Col span={24} className="logo-login">
            <img src={logo} className="logomobile" alt="img"></img>
          </Col>
          <Row>
            <Col className="welcome-back" span={24}>
              Welcome Back!
            </Col>
          </Row>

          <Row justify="center">
            <Col className="login-to">Login to your edly Account</Col>
          </Row>
          <Form className="login-form" onFinish={loginStudent}>
            <Row>
              <Col span={24}>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      type: "email",
                      message: "The input is not valid E-mail!",
                    },
                    {
                      required: true,
                      message: "Please input your E-mail!",
                    },
                  ]}
                >
                  <Input
                    type="email"
                    size="large"
                    className="emailinput university"
                    placeholder="E-mail"
                    name="email"
                    prefix={<MailOutlined size="large" className="mail-icon" />}
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <Form.Item
                  name="password"
                  rules={[{ required: true, message: "Please enter password" }]}
                >
                  <Input.Password
                    size="large"
                    className="passinput university"
                    placeholder="Password"
                    name="password"
                    prefix={
                      <UnlockOutlined size="large" className="mail-icon" />
                    }
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Link className="forgot-password" to="/student/forgot-password">
                  Forgot Password
                </Link>
              </Col>
            </Row>

            <Row justify="center">
              <Col className="login" span={24}>
                <Form.Item name="btn">
                  <Button
                    type="primary"
                    className="login-button-login"
                    size="large"
                    htmlType="submit"
                  >
                    Login
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>

          <Row>
            <Col span={24}>
              <div className="or">Or</div>
            </Col>
          </Row>

          <Row justify="center">
            <Col span={24}>
              <div className="dont-have-account">
                Don't have an account? <Link to="/student/signup">Sign up</Link>{" "}
                now.
              </div>
            </Col>
          </Row>
        </Col>
      </div>
    </div>
  );
};

export default withRouter(WelcomeStudent);
