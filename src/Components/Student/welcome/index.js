import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import {Link, Route} from 'react-router-dom';
import logo from "../../../Assets/images/Logo/Logo.svg";
import go_back from "../../../Assets/images/send-forward.png";

import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,

} from "antd";
import {useHistory} from "react-router-dom";
const { Content } = Layout;

function WelcomeMobile({history}) {
    return (
          <div className="welcome-desktop">
            <div className="go-back">
            <Button type="text"  onClick={history.goBack}>
              <span className="arrow-back">
            <Image src={go_back} preview={false} /></span>
           <span className="back-go"> Go Back</span>
          </Button>
            </div>
          <div className="mobileBackground">
           <Col className="view">

            <Col  span={24} className="logo-welcome">
                <img src={logo}  className="logowelcome">
                </img>
            </Col>
            
            </Col>
            <Row justify="center">
              <Col className="welcome-welcome">
                Welcome!
              </Col>
            </Row>
            <Row justify="center">
              <Col className="invested">
              invested in student success
              </Col>
            </Row>

            <Row justify="center">
              <Col className="login-welcome"  span={24}>
              <Link to="/student/login"><Button type="primary" className="login-button" size="large">Login</Button></Link>
              </Col>
            </Row>

            <Row justify="center">
              <Col className="signup-welcome"  span={24}>
              <Link to="/student/signup"><Button type="default" className="signup-button" size="large">Sign Up</Button></Link>
              </Col>
            </Row>
          </div>
          </div>
    );
  }
export default WelcomeMobile;
