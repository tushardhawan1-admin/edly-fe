import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
} from "antd";
import logo from "../../../../Assets/images/Logo/Logo.png";
import { Link } from "react-router-dom";
const { Footer } = Layout;

class Foot extends Component {
  state = {};
  render() {
    return (
      <Layout.Footer className="footer-student">
          
              <Divider/>
            <Row>
                <Col span={8}>
                <div className="logo-footer-student">
                  <Link to="/student/landing">
                    <Image src={logo} preview={false} className="logo-footer"/>
                    </Link>
                </div>
                </Col>
                <Col span={16}>
                <p className="copy-footer-student">
                All rights reserved by edly @ 2021
                </p>
                </Col>
            </Row>
          
              <Divider/>
          
      </Layout.Footer>
    );
  }
}
export default Foot;
