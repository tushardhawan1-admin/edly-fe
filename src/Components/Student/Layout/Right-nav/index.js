import {
  BookOutlined,
  DollarCircleOutlined,
  FileDoneOutlined,
  HomeOutlined,
  PoweroffOutlined,
} from "@ant-design/icons";

import { Link, withRouter } from "react-router-dom";
import { Col, Image, Layout, Menu, Row } from "antd";
import "antd/dist/antd.css";
import React, { Component } from "react";
import profile from "../../../../Assets/images/student-profile-image.png";
import "./index.css";

const { SubMenu } = Menu;

const { Header } = Layout;

class Right_nav extends Component {
  state = {};
  render() {
    console.log(this.props);
    return (
      <div>
        <div className="student">
          <div className="student-title">
            <Row className="profile-head" justify="space-between">
              {/* <Link to="/student/profile" onClick={this.props.onClickMenu} className="right_nav-text"> */}
              <Col
                span={4}
                className="nav-student-name"
                onClick={() => {
                  this.props.history.push("/student/profile");
                  this.props.onClickMenu();
                }}
              >
                <Image src={profile} preview={false} className="nav-profile" />
              </Col>
              <Col
                span={12}
                className="nav-student-name"
                onClick={() => {
                  this.props.history.push("/student/profile");
                  this.props.onClickMenu();
                }}
              >
                <span className="nav-name">
                  {localStorage.getItem("fname")}
                  <br /> {localStorage.getItem("lname")}
                </span>
              </Col>
              {/* </Link> */}

              <Col span={6}>
                <span className="logout">
                  <PoweroffOutlined />
                  &nbsp; Logout
                </span>
              </Col>
            </Row>
          </div>

          <Menu
            mode="inline"
            theme="light"
            defaultOpenKeys={["sub1"]}
            className="navmenu"
          >
            <Menu.Item key="1" icon={<HomeOutlined className="right_nav-icon"/>}>
              <Link to="/student/landing" onClick={this.props.onClickMenu} className="right_nav-text">
                Home
              </Link>
            </Menu.Item>
            <Menu.Item key="2" icon={<FileDoneOutlined className="right_nav-icon"/>}>
              <Link to="/student/how-it-works" onClick={this.props.onClickMenu} className="right_nav-text">
                How it Works
              </Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<DollarCircleOutlined className="right_nav-icon"/>}>
              <Link
                to="/student/change-funding"
                onClick={this.props.onClickMenu} className="right_nav-text"
              >
                Seek Investment
              </Link>
            </Menu.Item>
            <Menu.Item key="4" icon={<DollarCircleOutlined className="right_nav-icon"/>}>
              <Link to="/student/my-isa" onClick={this.props.onClickMenu} className="right_nav-text">
                My ISAs
              </Link>
            </Menu.Item>
            <SubMenu
              // key="sub1"
              icon={<BookOutlined className="right_nav-icon"/>}
              title="Resources"
              className="navmenu right_nav-text"
            >
              <Menu.Item key="5">
                <Link to="/student/resources" onClick={this.props.onClickMenu} className="right_nav-text right_nav-subtext">
                  About Student ISAs
                </Link>
              </Menu.Item>
              <Menu.Item key="6">
                <Link
                  to="/student/resources-media"
                  onClick={this.props.onClickMenu} className="right_nav-text right_nav-subtext"
                >
                  Media & Blogs
                </Link>
              </Menu.Item>
              <Menu.Item key="7">
                <Link
                  to="/student/resources-scholarships"
                  onClick={this.props.onClickMenu} className="right_nav-text right_nav-subtext"
                >
                  Scholarships
                </Link>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </div>
      </div>
    );
  }
}
export default withRouter(Right_nav);
