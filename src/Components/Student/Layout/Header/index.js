import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
} from "antd";
import logo from "../../../../Assets/images/Logo/Logo.png";
import menu from "../../../../Assets/images/menu.png";
import { Link, Route } from "react-router-dom";

const { Header } = Layout;

class Head extends Component {
  state = {};
  render() {
    console.log(this.props)
    return (
      <Header className="head-student">
        <Row>
          <Col span={12}>
            <div className="logo-header-student">
              {/* <Link to="/student/landing"> */}
              <Image src={logo} preview={false} className="logo-header" />
              {/* </Link> */}
            </div>
            
          </Col>
          <Col span={12} >
            <div className="menu-header-student" >
              <Image src={menu} preview={false} className="menu-header" onClick={this.props.onClickMenu}/>
            </div>
          </Col>
        </Row>
      </Header>
    );
  }
}
export default Head;
