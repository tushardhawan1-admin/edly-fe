import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
  Select,
  Steps,
  Collapse,
} from "antd";

import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  BankOutlined,
  BookOutlined,
  UnlockOutlined,
  FlagOutlined,
} from "@ant-design/icons";
import Filled_circle from "../profile_symbols/filled_circle/index";

import user_icon from "../../../Assets/images/user-active.png";
import mail_icon from "../../../Assets/images/mail-letter-active.png";
import school_icon from "../../../Assets/images/school-active.png";
import library_icon from "../../../Assets/images/library-active.png";
import unlock_icon from "../../../Assets/images/padlock-unlock-active.png";
import profile_student from "../../../Assets/images/student-profile-image.png";
import edit from "../../../Assets/images/edit-pencil.png";

const { Panel } = Collapse;
const { Option } = Select;

function callback(key) {
  console.log(key);
}

class Student_Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginContent: false,
    };
  }
  showContent = () => {
    this.setState({
      loginContent: !this.state.loginContent,
    });
    console.log("clicked");
  };
  render() {
    const panelHeader1 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={user_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">Personal Info</p>
      </div>
    );

    const panelHeader2 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={mail_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">Contact Info</p>
      </div>
    );

    const panelHeader3 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={school_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">Educational Info</p>
      </div>
    );

    const panelHeader4 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={library_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">ISA Applications</p>
      </div>
    );

    const panelHeader5 = () => (
      <div className="panel-header">
        <div
          className="form-icon-background"
          style={{ display: "inline-block" }}
        >
          <Image src={unlock_icon} className="form-icon" preview={false} />
        </div>
        <p className="student-form-title">Change Password</p>
      </div>
    );
    return (
      <div className="desktop-profile-student">
        <div className="profile-content">
          <Row>
            <Col span={24} className="student-profile-image">
              <Image
                src={profile_student}
                preview={false}
                className="profile-image"
              />
              <Image src={edit} preview={false} className="edit" />
            </Col>
          </Row>

          <Row>
            <Col span={24} className="student-name">
              <p>Hi! Aliss Bardet</p>
            </Col>
          </Row>
          
          <div className="student-details">
            <div className="status">
              <Row>
                <Col span={24}>
                  <Row>
                    <Col className="reg-no" span={17}>
                      #9103773457
                    </Col>
                    <Col className="application-status" span={7}>
                      <span className="red-dot">&#9679;</span>
                      <span>Pending</span>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24} className="student-amount">
                      $ 15,000
                    </Col>
                  </Row>

                  <Row>
                    <Col span={18} className="student-university">
                      University of Illinois at Urbana, Champagne
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>


            <div className="status">
              <Row>
                <Col span={24}>
                  <Row>
                    <Col className="reg-no" span={17}>
                      #9103773457
                    </Col>
                    <Col className="application-status" span={7}>
                      <span className="red-dot">&#9679;</span>
                      <span>Pending</span>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24} className="student-amount">
                      $ 15,000
                    </Col>
                  </Row>

                  <Row>
                    <Col span={18} className="student-university">
                      University of Illinois at Urbana, Champagne
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>

            <div className="status">
              <Row>
                <Col span={24}>
                  <Row>
                    <Col className="reg-no" span={17}>
                      #9103773457
                    </Col>
                    <Col className="application-status" span={7}>
                      <span className="red-dot">&#9679;</span>
                      <span>Pending</span>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24} className="student-amount">
                      $ 15,000
                    </Col>
                  </Row>

                  <Row>
                    <Col span={18} className="student-university">
                      University of Illinois at Urbana, Champagne
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
            
            </div>

            
        </div>
      </div>
    );
  }
}
export default Student_Profile;
