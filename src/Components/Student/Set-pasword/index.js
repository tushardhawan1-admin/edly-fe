import { UnlockOutlined } from "@ant-design/icons";
import { Button, Col, Form, Image, Input, Layout, Row } from "antd";
import "antd/dist/antd.css";
import axios from "axios";
import React from "react";
import { Link } from "react-router-dom";
import logo from "../../../Assets/images/Logo/Logo.svg";
import go_back from "../../../Assets/images/send-forward.png";
import "./index.css";

const { Content } = Layout;

function Forgot_Password(props) {
  function setPassword(values) {
    console.log("function");
    let userhash = window.location.pathname;
    const apiurl = `http://3.141.53.156/api/v1${userhash}`;

    const api_data = { ...values };
    console.log(api_data);
    let val = new FormData();
    Object.keys(api_data).forEach((key) => val.append(key, api_data[key]));
    console.log("fd", val);
    let res = "";
    let id = "";
    axios
      .patch(apiurl, val)
      .then((result) => {
        console.log("User Logged In", result);
        res = result.data.status;
        console.log(res);
        localStorage.setItem("status", res);
        console.log(localStorage.getItem("status"));
        props.history.push("/student/login");
      })
      .catch((error) => console.log(error));
    console.log(res);
  }

  return (
    <div className="forgot-password-desktop">
      <div className="go-back">
        <Button type="text" onClick={props.history.goBack}>
          <Image src={go_back} preview={false} />
          <span className="back-go"> Go Back</span>
        </Button>
      </div>
      <div className="forgot-password-background">
        <Col className="view">
          <Col className="logo-signup" span={24}>
            <img src={logo} className="logomobile"></img>
          </Col>
          <Row>
            <Col className="set-password-1" span={24}>
              Set Password
            </Col>
          </Row>

          <Row justify="center">
            <Col className="create-your">{/* Create your edly Account */}</Col>
          </Row>

          <Form className="password-form" onFinish={setPassword}>
            <Row>
              <Col span={24}>
                <Form.Item
                  name="password"
                  hasFeedback
                  rules={[
                    { required: true, message: "Please enter password" },
                    { min: 8, message: "Please enter at least 8 charecters" },
                  ]}
                >
                  <Input.Password
                    size="large"
                    className="email-input-password1 university"
                    placeholder="Password"
                    name="password"
                    prefix={<UnlockOutlined size="large" className="mailico" />}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Form.Item
                  name="confirm_password"
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: "Please confirm your password!",
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue("password") === value) {
                          return Promise.resolve();
                        }

                        return Promise.reject(
                          "The two passwords that you entered do not match!"
                        );
                      },
                    }),
                  ]}
                  dependencies={["password"]}
                >
                  <Input.Password
                    size="large"
                    className="email-input-password2 university"
                    placeholder="Confirm Password"
                    name="confirm_password"
                    prefix={<UnlockOutlined size="large" className="mailico" />}
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row justify="center">
              <Col className="submit-password" span={24}>
                <Button
                  type="primary"
                  className="submit-button-password"
                  size="large"
                  htmlType="submit"
                >
                  Submit
                </Button>
              </Col>
            </Row>
          </Form>

          {/* <Row justify="center">
            <Col className="login-now" span={24}>
              <Link to="/student/login">Login</Link> now
            </Col>
          </Row> */}
          <div style={{height:'30px'}}></div>

        </Col>
      </div>
    </div>
  );
}
export default Forgot_Password;
