import { MailOutlined, MobileOutlined, UserOutlined } from "@ant-design/icons";
import {
  Button,
  Checkbox,
  Col,
  Form,
  Image,
  Input,
  Layout,
  message,
  Row,
  Select,
} from "antd";
import "antd/dist/antd.css";
import React from "react";
import { Link } from "react-router-dom";
import API_MANAGER from "../../../API/index";
import logo from "../../../Assets/images/Logo/Logo.svg";
import go_back from "../../../Assets/images/send-forward.png";
import "./index.css";

const { Content } = Layout;
const { Option } = Select;

const WelcomeMobile = (props) => {


  const signup_student = (values) => {
    delete values["condition"];
    values["type"] = 10;
    console.log("values for post", values);
    API_MANAGER.signup(values)
      .then((result) => {
        console.log("User Created", result);
        message.success(
          "Signed up successfully. Please verify the account from your registered email."
        );
      })
      .catch((error) => console.log(error));
  };

  
  return (
    <div className="signup-desktop">
      <div className="go-back">
        <Button type="text" onClick={props.history.goBack}>
          <Image src={go_back} preview={false} />
          <span className="back-go"> Go Back</span>
        </Button>
      </div>
      <div className="signupBackground">
        <Col className="view">
          <Col className="logo-signup" span={24}>
            <img src={logo} className="logomobile" alt="img"></img>
          </Col>
          <Row>
            <Col className="welcome-signup" span={24}>
              Welcome
            </Col>
          </Row>

          <Row justify="center">
            <Col className="create-your">Create your edly Account</Col>
          </Row>

          <Form className="signup-form" onFinish={signup_student}>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="first_name"
                  rules={[
                    {
                      required: true,
                      message: "Please input your First Name!",
                    },
                  ]}
                >
                  <Input
                    type="text"
                    name="first_name"
                    size="large"
                    className="firstinput university"
                    placeholder="First Name"
                    prefix={<UserOutlined size="large" className="mail-icon" />}
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="last_name"
                  rules={[
                    { required: true, message: "Please input your Last Name!" },
                  ]}
                >
                  <Input
                    type="text"
                    size="large"
                    name="last_name"
                    className="firstinput university"
                    placeholder="Last Name"
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row>
              <Col span={24} className="country-code">
                <Form.Item
                  name="email"
                  rules={[
                    {
                      type: "email",
                      message: "The input is not valid E-mail!",
                    },
                    {
                      required: true,
                      message: "Please input your E-mail!",
                    },
                  ]}
                >
                  <Input
                    type="email"
                    size="large"
                    name="email"
                    className="phone-input university"
                    placeholder="E-mail"
                    prefix={<MailOutlined className="mail-icon" />}
                  />
                </Form.Item>
              </Col>
            </Row>

            <Row >
              {/* <Col span={12} className="country-code">
                <div className="custom-icon-wrapper">
                  <Form.Item name="country_code">
                    <Select
                      size="large"
                      name="country_code"
                      bordered={false}
                      defaultActiveFirstOption
                      placeholder="+01"
                      className="phone-input university"
                    >
                      <Option key="+01">+01</Option>
                      <Option key="+91">+91</Option>
                    </Select>
                  </Form.Item>
                  <MobileOutlined className="countrycode custom-icons" />
                </div>
              </Col> */}
              <Col span={24}>
              {/* <div className="custom-icon-wrapper"> */}
                <Form.Item
                  name="mobile"
                  rules={[
                    {
                      required: true,
                      message: "Please input your phone number!",
                    },
                  ]}
                >
                  <Input
                    type="number"
                    size="large"
                    name="mobile"
                    className="phone-input university"
                    placeholder="Phone Number"
                    prefix={<MobileOutlined className="main-icon" />}
                  />
                </Form.Item>
                {/* <MobileOutlined className="countrycode custom-icons" /> */}
                {/* </div> */}
              </Col>
            </Row>

            <Row className="check-box">
              <Col span={24}>
                <Form.Item
                  name="condition"
                  valuePropName="checked"
                  rules={[
                    {
                      required: true,
                      message: "Please accept the terms before proceeding",
                    },
                  ]}
                >
                  <Checkbox name="condition" value="accepted">
                    <span>
                      I have read and accepted the terms and conditions
                      published by edly.co
                    </span>
                    <Link className="read">Read all</Link>
                  </Checkbox>
                </Form.Item>
              </Col>
            </Row>

            <Row justify="center">
              <Col className="signup" span={24}>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="signup-button-signup"
                  size="large"
                >
                  Sign Up
                </Button>
                
              </Col>
            </Row>
          </Form>

          <Row>
            <Col span={24}>
              <div className="or">Or</div>
            </Col>
          </Row>

          <Row justify="center">
            <Col span={24}>
              <div className="already-have-account">
                Already have and account ?{" "}
                <Link to="/student/login">Login</Link> now.
              </div>
            </Col>
          </Row>
        </Col>
      </div>
    </div>
  );
};
export default WelcomeMobile;
