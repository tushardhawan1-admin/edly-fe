import "antd/dist/antd.css";
import "./index.css";
import {
  BankOutlined,
  DownOutlined,
  BookOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
  Select,
  Steps,
} from "antd";
import Empty_circle from "../table-symbols/empty_circle/index";
import Filled_circle from "../table-symbols/filled_circle/index";

import landing_image from "../../../Assets/images/landing-image.png";
import landing_image2 from "../../../Assets/images/landing-image-2.png";
import landing_image3 from "../../../Assets/images/landing-image-3.png";

import dollar_bag from "../../../Assets/images/bag-dollar.png";
import user_heart from "../../../Assets/images/user-heart.png";
import stocks_up from "../../../Assets/images/stocks-up.png";
import journal from "../../../Assets/images/journal-pencil.svg";

import rising_1 from "../../../Assets/images/rising-graph-1.png";
import rising_2 from "../../../Assets/images/rising-graph-2.png";

const { Content } = Layout;
const { Option } = Select;
const { Step } = Steps;

class Resources extends Component {
  isaForm = React.createRef();
  state = {
    current: 0,
  };

  onChange = (current) => {
    console.log("onChange:", current);
    this.setState({ current });
  };

  render() {
    const { current } = this.state;
    const desc_title = [
      "Apply",
      "Get Approved",
      "Receive Funding",
      "Go to school",
      "Get a Job",
      "Never Stress",
    ];
    const descinfo = [
      "Apply to see if you're eligible in 1 minute or less and don't worry, your application information will never effect your credit score.",
      "Edly processes applications and returns students with ISA contract terms. Once you're approved, Edly sets you up with your account servicer.",
      "Edly Sends directly to your school. You don't need to lift a finger.",
      "Keep your head down and your grades up! Focus on your Education without ever paying a dime.",
      "Once you land a job making above a defined minimum (Usually $30-40K per year) You'll Start paying a percentage of your income back to Edly.",
      "If life throws lemons, Your ISA payments are put on hold.",
    ];
    const desc = (i) => (
      <>
        <Image src={journal} style={{ fontSize: "20px" }} />
        <p>{descinfo[i]}</p>
      </>
    );

    return (
      <div className="desktop-landing">
        <div className="main-content">
          <Col xs={24} md={12} lg={12}>
            <Row>
              <Col xs={24} className="focus-column">
                <p className="focus">
                  ISAs 101: Understanding and Investing in Income Share
                  Agreements
                </p>
              </Col>
            </Row>
          </Col>

          <Row>
            <Col className="title" span={24}>
              How ISAs work
            </Col>
          </Row>
          <div className="how-isas">
            <Row>
              <Col span={24} className="feature-desc">
                Instead of taking out a loan, students agree to pay a fixed
                percentage of their earnings for a fixed number of months, which
                customarily ranges from 36 to 120 months (but can be more or
                less). Students are obligated to make a monthly payment only if
                they are working in that month and earning more than a specified
                minimum salary, which typically ranges from $25,000 to $60,000
                For example, a student may have to pay 10% of their earnings
                each month for 48 months, but only if they earn over $45,000 per
                year. Payments are also subject to a total cap on payments– for
                example, the student won’t need to pay more than 1.4 times the
                tuition (sometimes caps are higher or lower than that
                multiplier). There is also a payment obligation window after
                which the student no longer has any obligation to make further
                payments, regardless of the number or amount of payments made.
                The two charts below illustrate the cumulative payments made
                overtime by the student under a private student loan vs a
                $40,000 ISA salary (Graph A) and a private student loan vs a
                $60,000 ISA salary (Graph B). In both cases the private student
                loan requires the student to start paying earlier than an ISA
                and results in payments over a longer period of time. In
                contrast the ISA delays payments until the student starts
                earning the minimum income threshold and caps the student at a
                total number of payments and a total payment amount. Graph A
                represents a student earning $40,000 and demonstrates how the
                student benefits greatly compared to the payments it would have
                owed under a private student loan. ISA payments were not
                required until the student’s salary met the minimum income
                threshold and no further payments were due after the 60 month
                payment window. Graph B depicts the cashflows for a student
                earning above the minimum income threshold at the start. The
                student pays up to the cap in month 49 and no longer owes
                payments under the ISA.
              </Col>
            </Row>
          </div>
          <div className="graphs">
            <Row>
              <Col xs={24} md={12}>
                <Image src={rising_1} preview={false} />
              </Col>
              <Col xs={24} md={12}>
                <Image src={rising_2} preview={false} />
              </Col>
            </Row>
          </div>

          <Row>
            <Col className="title" span={24}>
              Sample ISA Terms
            </Col>
          </Row>
          <div className="how-isas">
            <Row>
              <Col span={24} className="feature-desc">
                edly believes that ISAs, in many circumstances, are the more
                affordable and flexible option when compared to private student
                loans . To prove this to schools and students, edly provides
                schools with analytic tools to compare private loans with ISAs.
                Based on our analysis, those who pay the maximum capped amount
                would often pay about the same as they would under a private
                student loan, but they benefit from the flexibility of an ISA.
                In effect, they get an “insurance” policy in case they cannot
                find a job or lose their job. However the average earner (and
                certainly low earners) with an ISA pay less than they would pay
                with private student loans. This analysis assumes that there is
                no co-signer. Depending on student circumstances (and especially
                if a parent co-signs a private student loan) a student may pay
                less in repayment of a loan than he or she would pay in the
                aggregate for an ISA.
              </Col>
            </Row>
          </div>

          <div className="estimated-isa">
            <Row>
              <Col className="title" span={24}>
                Your estimated ISA terms are:
              </Col>
            </Row>

            <Row>
              <Col className="sub-title" span={24}>
                ISA Percentage
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                5.50%
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Payments are based on a fixed percentage of your income so that
                you never pay more than you can afford.
              </Col>
            </Row>

            <Row>
              <Col className="isa-title-content" span={24}>
                Minimum Income Threshold
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                $40,000/Month
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Payments are based on a fixed percentage of your income so that
                you never pay more than you can afford.
              </Col>
            </Row>

            <Row>
              <Col className="isa-title-content" span={24}>
                Maximum Payments
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                60
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Maximum number of months with qualified payments towards your
                ISA Maximum Repayment Amount: [$2x funding amount]
              </Col>
            </Row>

            <Row>
              <Col className="isa-title-content" span={24}>
                Payment Window
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                12 months
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Many existing education financing options create an unfair
                burden for students. ISAs ensure that if you do not succeed up
                to a certain level, your payment amount is less or zero.
              </Col>
            </Row>

            <Row>
              <Col className="isa-title-content" span={24}>
                Maximum Repayment
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                2x Funding Amount
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Never pay above the defined maximum repayment amount. Your
                contract is fulfilled once either maximum (Payments or Repayment
                Amount) is met, or at the end of your payment window.
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}
export default Resources;
