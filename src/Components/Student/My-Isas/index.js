import {
  BankOutlined,
  CalendarOutlined,
  FlagOutlined,
  MailOutlined,
  ReadOutlined,
  UnlockOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  Button,
  Col,
  Collapse,
  DatePicker,
  Divider,
  Form,
  Image,
  Input,
  Row,
  Select,
} from "antd";
import "antd/dist/antd.css";
import moment from "moment";
import React, { Component } from "react";
import API_Manager from "../../../API/index";
import edit from "../../../Assets/images/edit-pencil.png";
import library_icon from "../../../Assets/images/library-active.png";
import mail_icon from "../../../Assets/images/mail-letter-active.png";
import unlock_icon from "../../../Assets/images/padlock-unlock-active.png";
import school_icon from "../../../Assets/images/school-active.png";
import profile_student from "../../../Assets/images/student-profile-image.png";
import user_icon from "../../../Assets/images/user-active.png";
import "./index.css";

const { Panel } = Collapse;
const { Option } = Select;

function callback(key) {
  console.log(key);
}

class Student_Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginContent: false,
    };
  }

  studentIsa = () => {
    API_Manager.studentIsa(localStorage.getItem("student_id"))
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  render() {
    return (
      <div
        className="desktop-my-isa-student"
        // onLoad={(e) => this.studentRetrieve(e)}
      >
        <div className="profile-content-isa">
          <Row>
            <Col span={24} className="student-profile-image">
              <Image
                src={profile_student}
                preview={false}
                className="profile-image"
              />
            </Col>
          </Row>

          <Row>
            <Col span={24} className="student-name">
              <p>
                {" "}
                Hi! &nbsp;
                {localStorage.getItem("fname")}&nbsp;
                {localStorage.getItem("lname")}
              </p>
            </Col>
          </Row>

          <Row className="isa-stats-my">
            <Col span={8}>
              <div className="isa-no">3</div>
              <div className="isa-status-text">Active ISAs</div>
            </Col>
            <Col span={8}>
              <div className="isa-no">1</div>
              <div className="isa-status-text">Pending ISA</div>
            </Col>
            <Col span={8}>
              <div className="isa-no">1</div>
              <div className="isa-status-text">Active ISAs</div>
            </Col>
          </Row>
          <Row className="drafts">
            <Col span={24}>
              <span>Drafts</span>
            </Col>
          </Row>

          <div className="drafted">
            <Row>
              <Col span={18} className="draft-date">
                10 July
              </Col>
              <Col span={6}>
                <span className="edit-draft">Draft</span>
              </Col>
            </Row>

            <Row className="draft-amount-row">
              <Col className="draft-isa-amount" span={12}>
                $ 15000
              </Col>
              <Col className="draft-school" span={12}>
                Northeastern University
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Button
                  htmlType="submit"
                  type="primary"
                  className="continue-app"
                >
                  Continue Application
                </Button>
              </Col>
            </Row>
          </div>

          <div className="status-lists">
            <div className="status-isa">
              <Row>
                <Col span={24}>
                  <Row>
                    <Col className="reg-no" span={17}>
                      #9103773457
                    </Col>
                    <Col className="application-status" span={7}>
                      <span className="red-dot">&#9679;</span>
                      <span>Pending</span>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24} className="student-amount">
                      $ 15,000
                    </Col>
                  </Row>

                  <Row className="university-date-row" align="bottom">
                    <Col span={18} className="student-university">
                      University of Illinois at Urbana, Champagne
                    </Col>
                    <Col span={6} className="student-isa-date">
                      10 July 2019
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
            <div className="status-isa">
              <Row>
                <Col span={24}>
                  <Row>
                    <Col className="reg-no" span={17}>
                      #9103773457
                    </Col>
                    <Col className="application-status" span={7}>
                      <span className="red-dot">&#x1f7e2;</span>
                      <span>Pending</span>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24} className="student-amount">
                      $ 15,000
                    </Col>
                  </Row>

                  <Row className="university-date-row" align="bottom">
                    <Col span={18} className="student-university">
                      University of Illinois at Urbana, Champagne
                    </Col>
                    <Col span={6} className="student-isa-date">
                      10 July 2019
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
            <div className="status-isa">
              <Row>
                <Col span={24}>
                  <Row>
                    <Col className="reg-no" span={17}>
                      #9103773457
                    </Col>
                    <Col className="application-status" span={7}>
                      <span className="red-dot">&#9679;</span>
                      <span>Pending</span>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24} className="student-amount">
                      $ 15,000
                    </Col>
                  </Row>

                  <Row className="university-date-row" align="bottom">
                    <Col span={18} className="student-university">
                      University of Illinois at Urbana, Champagne
                    </Col>
                    <Col span={6} className="student-isa-date">
                      10 July 2019
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Student_Profile;
