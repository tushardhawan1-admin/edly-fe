import "antd/dist/antd.css";
import "./index.css";
import {
  BankOutlined,
  DownOutlined,
  BookOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
  Select,
  Steps,
} from "antd";

import image1 from "../../../Assets/images/how-it-works.png";
import image2 from "../../../Assets/images/plant-1.png";
import how_it_works from "../../../Assets/images/how-it-works-graph.png";
import graph1 from "../../../Assets/images/why-edly.png";

import Empty_circle from "../table-symbols/empty_circle/index";
import Filled_circle from "../table-symbols/filled_circle/index";

import landing_image from "../../../Assets/images/landing-image.png";
import landing_image2 from "../../../Assets/images/landing-image-2.png";
import landing_image3 from "../../../Assets/images/landing-image-3.png";

import dollar_bag from "../../../Assets/images/dollar-bag-filled.png";
import user_heart from "../../../Assets/images/user-heart-filled.png";
import stocks_up from "../../../Assets/images/stocks-filled.png";
import journal from "../../../Assets/images/journal-pencil.svg";

const { Content } = Layout;
const { Option } = Select;
const { Step } = Steps;

class Landing_Page extends Component {
  isaForm = React.createRef();
  state = {
    current: 0,
  };

  onChange = (current) => {
    console.log("onChange:", current);
    this.setState({ current });
  };

  render() {
    const { current } = this.state;
    const desc_title = [
      "Apply",
      "Get Approved",
      "Receive Funding",
      "Go to school",
      "Get a Job",
      "Never Stress",
    ];
    const descinfo = [
      "Apply to see if you're eligible in 1 minute or less and don't worry, your application information will never effect your credit score.",
      "Edly processes applications and returns students with ISA contract terms. Once you're approved, Edly sets you up with your account servicer.",
      "Edly Sends directly to your school. You don't need to lift a finger.",
      "Keep your head down and your grades up! Focus on your Education without ever paying a dime.",
      "Once you land a job making above a defined minimum (Usually $30-40K per year) You'll Start paying a percentage of your income back to Edly.",
      "If life throws lemons, Your ISA payments are put on hold.",
    ];
    const desc = (i) => (
      <>
        <Image src={journal} style={{ fontSize: "20px" }} />
        <p>{descinfo[i]}</p>
      </>
    );

    return (
      <div className="desktop-landing">
        <div className="main-content">
          <Row>
            <Col xs={24} md={12} lg={12}>
              <Image src={image1} preview={false} className="landing-image" />
            </Col>
            <Col xs={24} md={12} lg={12}>
              <Row>
                <Col xs={24} className="focus-column">
                  <p className="focus">
                    Stress of tuition payments, Not any more.
                  </p>
                </Col>
                <Col xs={24} className="check-button">
                  <Button type="primary" size="large" className="check-now">
                    Check Now
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col xs={24}>
              <Row>
                <Col span={24} className="what-is-isa">
                  <div className="isa"> What is ISA</div>
                </Col>
                <Col span={24} className="what-is-isa-desc">
                  <div className="isa-desc">
                    Instead of loans, edly offers ISAs – or Income Share
                    Agreements. With ISAs, you only pay for your tuition once
                    you land your first well-paying job. Payments adjust with
                    your income and are designed to be affordable.
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
          <div className="features">
            <Row>
              <Col span={24} className="title">
                <div className="feat">How ISAs Work</div>
                <Row gutter={{ xs: 8, sm: 16, md: 56, lg: 56 }}>
                  <Col xs={24} md={8} lg={8} className="image isa">
                    {/* <Image
                      src={dollar_bag}
                      preview={false}
                      className="image-icon"
                    /> */}
                    {/* <div className="feature-name">learn now, pay later</div> */}
                    <div className="feature-desc isa-working">
                      Instead of loans, edly offers ISAs – or Income Share
                      Agreements. With ISAs, you only pay for your tuition once
                      you land your first well-paying job. Payments adjust with
                      your income and are designed to be affordable. Instead of
                      loans, edly offers ISAs – or Income Share Agreements. With
                      ISAs, you only pay for your tuition once you land your
                      first well-paying job. Payments adjust with your income
                      and are designed to be affordable.
                    </div>

                    <div>
                      <Image src={how_it_works} preview={false} />
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div className="how-we-predict">
            <Row>
              <Col className="title" span={24}>
                How we predict your income
              </Col>
            </Row>

            <Row>
              <Col className="feature-desc" className={24}>
                Instead of loans, edly offers ISAs – or Income Share Agreements.
                With ISAs, you only pay for your tuition once you land your
                first well-paying job. Payments adjust with your income and are
                designed to be affordable. Instead of loans, edly offers ISAs –
                or Income Share Agreements. With ISAs, you only pay for your
                tuition once you land your first well-paying job. Payments
                adjust with your income and are designed to be affordable.
              </Col>
            </Row>
          </div>
          <Row>
            <Col span={24}>
              <Image src={image2} preview={false} className="landing-image" />
            </Col>
          </Row>
          <div className="why-is-isa-how">
            <Row>
              <Col span={24} className="title">
                <div className="feat">Why Is an ISA right for me?</div>
              </Col>
            </Row>

            <Row>
              <Col span={24} className="what-is-isa-desc">
                <div className="isa-desc">
                  Many existing education financing options create an unfair
                  burden for student. ISAs Ensure that if you do not succeed up
                  to a certain level your payment amount is less or zero.Many
                  existing education financing options create an unfair burden
                  for student. ISAs Ensure that if you do not succeed up to a
                  certain level your payment amount is less or zero.
                </div>
              </Col>
            </Row>
          </div>
        <div className="feature-of-edly">
          <Row>
            <Col span={24} className="landing-image">
              <Image
                src={graph1}
                preview={false}
                className="landing-image-3"
              />
            </Col>
          </Row>

          <Row>
            <Col span={24} className="title">
            Features of an Edly ISA
            </Col>
          </Row>
          <Row>
              <Col span={8}>
              <Image
                src={dollar_bag}
                preview={false}
                className="icons-how"
              />
              <div className="sub-title">
              Learn now Pay later
              </div>
              </Col>
          
              <Col span={8}>
              <Image
                src={user_heart}
                preview={false}
                className="icons-how"
              />
              <div className="sub-title">
              Tailored, for you
              </div>
              </Col>
          
              <Col span={8}>
              <Image
                src={stocks_up}
                preview={false}
                className="icons-how"
              />
              <div className="sub-title">
              Invest In Your success
              </div>
              </Col>
          </Row>


          
          </div>
          <Row>
            <Col span={24} className="check-button">
              <Button type="primary" size="large" className="check-now">
                Check Now
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
export default Landing_Page;
