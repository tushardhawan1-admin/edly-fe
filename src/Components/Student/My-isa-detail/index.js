import "antd/dist/antd.css";
import "./index.css";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
  Select,
  Steps,
  Collapse,
} from "antd";

import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  BankOutlined,
  BookOutlined,
  UnlockOutlined,
  FlagOutlined,
} from "@ant-design/icons";
import Filled_circle from "../profile_symbols/filled_circle/index";

import user_icon from "../../../Assets/images/user-active.png";
import mail_icon from "../../../Assets/images/mail-letter-active.png";
import school_icon from "../../../Assets/images/school-active.png";
import library_icon from "../../../Assets/images/library-active.png";
import unlock_icon from "../../../Assets/images/padlock-unlock-active.png";
import profile_student from "../../../Assets/images/student-profile-image.png";
import edit from "../../../Assets/images/edit-pencil.png";

import download from "../../../Assets/images/download.png";

const { Panel } = Collapse;
const { Option } = Select;

function callback(key) {
  console.log(key);
}

class Student_Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginContent: false,
    };
  }
  showContent = () => {
    this.setState({
      loginContent: !this.state.loginContent,
    });
    console.log("clicked");
  };
  render() {
    return (
      <div className="desktop-profile-student">
        <div className="profile-content">
          <div className="student-details">
          <div className="status-isa">
              <Row>
                <Col span={24}>
                  <Row>
                    <Col className="reg-no" span={17}>
                      #9103773457
                    </Col>
                    <Col className="application-status" span={7}>
                      <span className="red-dot">&#9679;</span>
                      <span>Pending</span>
                    </Col>
                  </Row>

                  <Row>
                    <Col span={24} className="student-amount">
                      $ 15,000
                    </Col>
                  </Row>

                  <Row className="university-date-row" align="bottom">
                    <Col span={18} className="student-university">
                      University of Illinois at Urbana, Champagne
                    </Col>
                    <Col span={6} className="student-isa-date">
                      10 July 2019
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
            <div className="isa-detailing">
              <Row>
                <Col className="agreement" span={22}>
                  ISA Agrement<div className="agreement-sub">View</div>
                </Col>
                <Col span={2}>
                  <Image src={download} preview={false} className="downlaod" />
                </Col>
              </Row>

              <Row>
                <Col className="title" span={24}>
                  ISA Details
                </Col>
              </Row>
              <Row>
                <Col span={24} className="isa-subtitle">
                  ISA Amount
                </Col>
                <Col span={24}>
                  <span className="isa-amt">$ 15,000</span>
                </Col>
              </Row>
              <Row>
                <Col span={24} className="isa-subtitle">
                  Income Share
                </Col>
                <Col span={24}>
                  <span className="isa-amt">2.75%</span>
                </Col>
              </Row>
              <Row>
                <Col span={24} className="isa-subtitle">
                  Required Payments
                </Col>
                <Col span={24}>
                  <span className="isa-amt">84</span>
                </Col>
              </Row>
              <Row>
                <Col span={24} className="isa-subtitle">
                  Minimum Income Threshold
                </Col>
                <Col span={24}>
                  <span className="isa-amt">$40,000.00</span>
                </Col>
              </Row>
              <Row>
                <Col span={24} className="isa-subtitle">
                  Payment Cap (Ceiling)
                </Col>
                <Col span={24}>
                  <span className="isa-amt">$30.894.00</span>
                </Col>
              </Row>

              <Row>
                <Col className="title" span={24}>
                  About Your ISA
                </Col>
              </Row>
              <Row>
                <Col className="abt-cont" span={24}>
                  <p>An Income Share Agreement "ISA" is not credit or a loan, and
                  the amount you will be required to pay: (i) may be more or
                  less than the ISA Amount provided to you; and (ii) will vary
                  in proportion to your future income. An ISA does not give us
                  any rights. regarding your choice of career or employment
                  pursuits.</p><p> Northeastern University has agreed to provide you
                  with access to H - $15,000 ISA - Accelerated BSN - Aug 2021
                  Graduation (the "Program"), in exchange for your promise to
                  pay the Income Share during the Contract Term.</p><p> Your payments
                  will be waived when you are earning less than $40,000.00 per
                  year, (equivalent to $3,333.33 on a monthly basis).</p>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Student_Profile;
