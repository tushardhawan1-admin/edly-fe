import "antd/dist/antd.css";
import "./index.css";
import {
  BankOutlined,
  DownOutlined,
  BookOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import React, { Component, useState } from "react";
import {
  Divider,
  Layout,
  Space,
  Typography,
  Menu,
  Row,
  Col,
  Button,
  Image,
  Input,
  Form,
  Dropdown,
  DatePicker,
  Select,
  Steps,
  Affix,
} from "antd";

import Empty_circle from "../table-symbols/empty_circle/index";
import Filled_circle from "../table-symbols/filled_circle/index";
import why_edly from "../../../Assets/images/why-edly.png";

const Isa_eligibility =()=> {
  const [container, setContainer] = useState(null);
    return (
      <div className="desktop-landing">
         <div className="scrollable-container" ref={setContainer}>
         <Affix target={() => container} className="affix-header">
         <Row>
              <Col className="isa-title-content" span={24}>
              $ 10,000
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                $40,000/Month
              </Col>
            </Row>
        </Affix>
        </div>
        <div className="main-content">
          
          <div className="estimated-isa">
            <Row>
              <Col className="title" span={24}>
                Your estimated ISA terms are:
              </Col>
            </Row>

            <Row>
              <Col className="sub-title" span={24}>
                ISA Percentage
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                5.50%
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Payments are based on a fixed percentage of your income so that
                you never pay more than you can afford.
              </Col>
            </Row>

            <Row>
              <Col className="isa-title-content" span={24}>
                Minimum Income Threshold
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                $40,000/Month
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Payments are based on a fixed percentage of your income so that
                you never pay more than you can afford.
              </Col>
            </Row>

            <Row>
              <Col className="isa-title-content" span={24}>
                Maximum Payments
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                60
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Maximum number of months with qualified payments towards your
                ISA Maximum Repayment Amount: [$2x funding amount]
              </Col>
            </Row>

            <Row>
              <Col className="isa-title-content" span={24}>
                Payment Window
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                12 months
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Many existing education financing options create an unfair
                burden for students. ISAs ensure that if you do not succeed up
                to a certain level, your payment amount is less or zero.
              </Col>
            </Row>

            <Row>
              <Col className="isa-title-content" span={24}>
                Maximum Repayment
              </Col>
            </Row>

            <Row>
              <Col className="percent" span={24}>
                2x Funding Amount
              </Col>
            </Row>

            <Row>
              <Col className="isa-content" span={24}>
                Never pay above the defined maximum repayment amount. Your
                contract is fulfilled once either maximum (Payments or Repayment
                Amount) is met, or at the end of your payment window.
              </Col>
            </Row>
          </div>
          <div className="why-is-isa">
            <Row>
              <Col span={24} className="title">
                ISA against traditional education loans
              </Col>
            </Row>

            <Row>
              <Col span={24} className="title-desc">
                Based on the inputs you provided, Edly predicts your salary will
                be $20,000 and that your monthly payments will look something
                like this:
              </Col>
            </Row>

            <div className="table">
              <Row className="table-title">
                <Col span={14} className="name">
                  <p className="title">Features</p>
                </Col>
                <Col span={5} className="edly">
                  <p className="title">Edly</p>
                </Col>
                <Col span={5} className="private-loans">
                  <p className="title">Private Loans</p>
                </Col>
              </Row>

              <Row>
                <Col span={14} className="name">
                  <p>High FICO score</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={5} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Co-signer</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={5} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Approval rate</p>
                </Col>
                <Col span={5} className="edly">
                  High
                </Col>
                <Col span={5} className="private-loans">
                  Low
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Interest rate</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={5} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>income-based repayment</p>
                </Col>
                <Col span={5} className="edly">
                  <Filled_circle color="#ffffff" />
                </Col>
                <Col span={5} className="private-loans">
                  <Empty_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Number of payments</p>
                </Col>
                <Col span={5} className="edly">
                  Fixed
                </Col>
                <Col span={5} className="private-loans">
                  Indefinite
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Maximum repayment amount</p>
                </Col>
                <Col span={5} className="edly">
                  Fixed
                </Col>
                <Col span={5} className="private-loans">
                  Indefinite
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Debt</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={5} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Minimum income threshold</p>
                </Col>
                <Col span={5} className="edly">
                  <Filled_circle color="#ffffff" />
                </Col>
                <Col span={5} className="private-loans">
                  <Empty_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Employment guarantee</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={5} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Auto-pay student discount</p>
                </Col>
                <Col span={5} className="edly">
                  <Empty_circle color="#ffffff" />
                </Col>
                <Col span={5} className="private-loans">
                  <Filled_circle color="#7e7e7e" />
                </Col>
              </Row>
              <Row>
                <Col span={14} className="name">
                  <p>Value signal/recruiting effectiveness</p>
                </Col>
                <Col span={5} className="edly">
                  High
                </Col>
                <Col span={5} className="private-loans last">
                  None
                </Col>
              </Row>
            </div>
          </div>

          <Row>
            <Col span={24} className="title">
              Why Edly?
            </Col>
          </Row>

          <Row>
            <Col span={24} className="title-desc">
              Here is an Example of how your payments would adjust on your month
              to month earnings, assuming 4.5% ISA rate and $30.000 minimum
              income.
            </Col>
          </Row>
          <div className="estimated-isa">
              <Image src={why_edly} preview={false}/>
          </div>

          <Row>
            <Col span={24} className="disclaimer-title">
              Disclaimer
            </Col>
          </Row>

          <Row>
            <Col span={24} className="disclaimer">
              The above mentioned stats have been gathered on the on the basesof
              general assumptions of Private school loans, the market rates may
              varie time to time. Edly doesn't ensures any grantees to the above
              mentioned rates for the private school loan. Your privet schools
              may be better or worst than what has been mentioned hear.
            </Col>
          </Row>
          
        </div>
      </div>
    );
  }
export default Isa_eligibility;
