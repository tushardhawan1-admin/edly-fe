import Welcome_I from "../Components/Investor/welcome/index";
import Signup_I from "../Components/Investor/signup/index";
import Password_I from "../Components/Investor/password/index";
import Layout from "../Components/Investor/index/index";

import Layout_S from "../Components/Student/index/index";
import Welcome_S from "../Components/Student/welcome/index";
import Login_S from "../Components/Student/login/index";
import Signup_S from "../Components/Student/signup/index";
import Forgot_Password_S from "../Components/Student/Forgot-password/index";
import Forgot_Password_S_1 from "../Components/Student/Forgot-Password-1/index";
import Landing_S from "../Components/Student/Landing_Page/index";
import Profile_S from "../Components/Student/Profile/index";
import Create_ISA_S from "../Components/Student/Create-Isa/index";
import Isa_eligibility from "../Components/Student/Isa-eligiblity/index";
import Change_Funding_S from "../Components/Student/Change-fuding/index";
import Isa_eligibility_no from "../Components/Student/Isa-eligibility-no/index";
import How_it_works from "../Components/Student/How-it-works/index";
import Resources_S from "../Components/Student/Resources/index";
import Resources_media_S from "../Components/Student/Resource-media/index";
import Resources_media_details_S from "../Components/Student/Resource-media-details/index";
import Resources_scholarships from "../Components/Student/Resources-scholarships/index";
import My_isa from "../Components/Student/My-isa/index";
import My_isa_detail from "../Components/Student/My-isa-detail/index";
import Set_Password from "../Components/Student/Set-pasword/index";
import Welcome1_S from "../Components/Student/welcome-1/index";
import myIsa_S from "../Components/Student/My-Isas/index";

import { BrowserRouter, Switch, Route, useHistory } from "react-router-dom";

function AppRoutes() {
  return (
    <BrowserRouter history={useHistory}>
      <Switch>
        <Route path="/student/login" component={Login_S} />
        <Route path="/student/signup" component={Signup_S} />
        <Route path="/student/welcome" component={Welcome_S} />
        <Route path="/student/welcome-1" component={Welcome1_S} />
        <Route path="/student/forgot-password" component={Forgot_Password_S} />
        <Route path="/users/create-password" component={Set_Password} />
        <Route
          path="/student/forgot-password-1"
          component={Forgot_Password_S_1}
        />

        <Route path="/investor">
          <Layout>
            <Route path="/investor/welcome" component={Welcome_I} />
            <Route path="/investor/signup" component={Signup_I} />
            <Route path="/investor/password" component={Password_I} />
          </Layout>
        </Route>

        <Route path="/student">
          <Layout_S>
            <Route exact path="/student/" component={Landing_S} />
            <Route exact path="/student/profile" component={Profile_S} />
            <Route exact path="/student/create-isa" component={Create_ISA_S} />
            <Route
              exact
              path="/student/isa-eligibility"
              component={Isa_eligibility}
            />
            <Route
              exact
              path="/student/change-funding"
              component={Change_Funding_S}
            />
            <Route
              exact
              path="/student/isa-eligibility-no"
              component={Isa_eligibility_no}
            />
            <Route
              exact
              path="/student/how-it-works"
              component={How_it_works}
            />
            <Route exact path="/student/resources" component={Resources_S} />
            <Route
              exact
              path="/student/resources-media"
              component={Resources_media_S}
            />
            <Route
              exact
              path="/student/resources-media-details"
              component={Resources_media_details_S}
            />
            <Route
              exact
              path="/student/resources-scholarships"
              component={Resources_scholarships}
            />
            <Route exact path="/student/my-isa" component={My_isa} />
            <Route
              exact
              path="/student/my-isa-detail"
              component={My_isa_detail}
            />
            <Route exact path="/student/my-isas" component={myIsa_S} />
          </Layout_S>
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default AppRoutes;
